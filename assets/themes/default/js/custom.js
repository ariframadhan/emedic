var pathArray = window.location.pathname.split('/');
var origin = window.location.origin;
if (origin == 'https://localhost' || origin == 'http://localhost') {
    var base_url = window.location.origin + '/' + pathArray[1] + '/';
} else {
    var base_url = window.location.origin + '/';
}

// console.log(base_url);

$(document).ready(function () {
    var $form = $("#form");
    var $input = $form.find(".thousand-separator");

    $input.on("keyup", function (event) {


        // When user select text in the document, also abort.
        var selection = window.getSelection().toString();
        if (selection !== '') {
            return;
        }

        // When the arrow keys are pressed, abort.
        if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
            return;
        }


        var $this = $(this);

        // Get the value.
        var input = $this.val();

        var input = input.replace(/[\D\s\._\-]+/g, "");
        input = input ? parseInt(input, 10) : 0;

        $this.val(function () {
            return (input === 0) ? "" : input.toLocaleString("id");
        });
    });
});

function tidy_datatable() {
    // make show entry form more pretty
    var show_entry = $('.dataTables_length');
    show_entry.parent('div').removeClass('col-sm-6').addClass('col-lg-2 col-md-2 col-sm-6 col-xs-6');
    show_entry.parent('div').prepend('<label class="nowrap">Tampilkan</label>');
    show_entry.children('label').children().appendTo('.dataTables_length');
    show_entry.parent('div').css('text-align', 'left');
    show_entry.closest('div').css('text-align', 'left');
    show_entry.children('select').css('width', '100%');

    // make search form more pretty
    var parent_css = {
        'position': 'absolute',
        'right': '-5px'
    };
    var style = {
        'width': '100%',
        'margin-left': '0px'
    };
    var search_area = $('.dataTables_filter');
    search_area.parent('div').css('text-align', 'left');
    search_area.parent('div').removeClass('col-sm-6').addClass('col-lg-2 col-md-3 col-sm-6 col-xs-6').prepend('<div class="search-area"></div>');
    search_area.children('label').children().appendTo('.dataTables_filter');
    search_area.parent('div').css(parent_css);
    search_area.parent('div').prepend('<label>Pencarian</label>');
    $('.dataTables_filter').children('input').css(style);

    // hide info page if small device
    $('.dataTables_info').addClass('hidden-sm hidden-xs');
    $('.dataTables_info').parent('div').parent('div').css('margin-top', '20px');

    // add line before table 
    search_area.parent('div').parent('div').append('<div class="col-xs-12 hidden-lg hidden-md"><div class="row"><hr class="no-padding" /></div></div>')

    //align center if datatable pagination under 11 column
    var page_elm = $('ul.pagination li').length;
    var max_elm = 11; // default max generated column by datatable 
    if (page_elm < max_elm) {
        var elm = $('.pagination');
        elm[0].style.setProperty('display', 'table', 'important');
        $('.pagination').css('margin', 'auto');
    }
}

function initTinymce(dom) {
    tinymce.init({
        selector: dom,  // change this value according to your HTML
        relative_urls: false,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime nonbreaking save table directionality",
            "emoticons template paste textpattern tabfocus responsivefilemanager tiny_mce_wiris media"
        ],
        height: "200px",
        toolbar: "insertfile undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager tiny_mce_wiris_formulaEditor media",
        remove_linebreaks: true,
        external_filemanager_path: base_url + "assets/themes/admin/filemanager/",
        filemanager_title: "File Manager",
        external_plugins: { "filemanager": base_url + "assets/themes/admin/filemanager/plugin.min.js" }
    });
}

function initTinymceToolbarOnly(dom, readonly = false) {
    tinymce.init({
        selector: dom,  // change this value according to your HTML
        relative_urls: false,
        readonly: readonly,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime nonbreaking save table directionality",
            "emoticons template paste textpattern tabfocus responsivefilemanager tiny_mce_wiris media"
        ],
        menubar: false, // Hide top Menubar
        statusbar: false, // Hide bottom Statusbar
        height: "200px",
        remove_linebreaks: true,
        toolbar: "insertfile undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager tiny_mce_wiris_formulaEditor media",

        external_filemanager_path: base_url + "assets/themes/admin/filemanager/",
        filemanager_title: "File Manager",
        external_plugins: { "filemanager": base_url + "assets/themes/admin/filemanager/plugin.min.js" }
    });
    // tinymce.activeEditor.setMode('readonly');
}

function generate_dropdown(type = 'periode', element, request_url, data_packet) {
    $.post(request_url, data_packet, function (result) {
        result = $.parseJSON(result);
        switch (type) {
            case 'periode':
                var opt = '<option value="" selected disabled> PILIH TAHUN </option>';
                $.each(result, function (key, val) {
                    opt += '<option value="' + val + '">' + val + '</option>';
                });
                break;

            case 'periode-transaksi':
                var opt = '<option value="" selected disabled> PILIH TAHUN </option>';
                $.each(result, function (key, val) {
                    opt += '<option value="' + val + '">' + val + '</option>';
                });
                break;

            case 'periode-lulusan':
                var opt = '<option value="" selected disabled> PILIH TAHUN </option>';
                $.each(result, function (key, val) {
                    opt += '<option value="' + val + '">' + val + '</option>';
                });
                break;

            case 'prodi':
                var opt = '<option value="" selected disabled> PILIH PROGRAM STUDI </option>';
                $.each(result, function (key, val) {
                    opt += '<option value="' + val['kd_prodi'] + '"> (' + val['jenjang_prodi'] + ') ' + val['nm_prodi'] + '</option>';
                });
                break;

            default:
                $.each(result, function (key, val) {
                    opt += '<option value="' + val + '">' + val + '</option>';
                });
                break;
        }
        opt += '<option value="all"> SEMUA </option>';
        element.html(opt);
    });
}

function onchange_filter(element, scope_field, url, table_element, callback, exception = []) {

    element.on('change', function (e) {
        e.stopImmediatePropagation();

        var filter = [];
        $.each(scope_field, function (key, value) {

            if (exception.length > 0) {
                if (element.hasClass(exception[0]) && value == ('.' + exception[1])) {
                    filter[key] = 'all';
                } else {
                    filter[key] = $(value).find(':selected').val();
                    filter[key] = filter[key] == '' ? 'all' : filter[key];
                }
            } else {
                filter[key] = $(value).find(':selected').val();
                filter[key] = filter[key] == '' ? 'all' : filter[key];
            }
        });

        var draw_url = url;
        $.each(filter, function (key, value) {
            draw_url = draw_url + value + '/';
        });

        table_element.ajax.url(draw_url).draw();

        if (typeof callback === 'function') {
            callback();
        }
    });

}

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};

function readUrl(input, preview_element = null) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = (e) => {
            let imgData = e.target.result;
            let imgName = input.files[0].name;
            input.setAttribute("data-title", imgName);

            if (preview_element != null) {
                preview_element.attr('src', imgData);
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function tgl_indo(international_date) {
    var date_array = international_date.split("-");
    var _tahun = date_array[0];
    var _bulan = date_array[1];
    var _hari = date_array[2];

    var bulan = { '01': 'Januari', '02': 'Februari', '03': 'Maret', '04': 'April', '05': 'Mei', '06': 'Juni', '07': 'Juli', '08': 'Agustus', '09': 'September', '10': 'Oktober', '11': 'November', '12': 'Desember' };
    var bulan = bulan[_bulan];

    return _hari + ' ' + bulan + ' ' + _tahun;
}

function getHari(date) {
    var weekday = ['MINGGU', 'SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU'];
    var dateObj = new Date(date);
    var hari = weekday[dateObj.getDay()];
    return hari;
}

function getCurrentTimeStamp() {
    var now = new Date();
    return now.getFullYear().toString() + "_" + now.getMonth().toString() + "_" + now.getDate().toString() + "_" + now.getHours().toString() + "_" + now.getMinutes().toString() + "_" + now.getSeconds().toString();
}

function strip_tags(string) {
    return string.toString().replace(/<[^>]*>?/gm, '');
}
