<?php
$id = $type == 'edit' ? $data['id_keuangan_harga'] : null;
?>

<form action="<?= base_url('admin/jenis-harga-keuangan/save-harga/'.$id) ?>" method="post" id="form-keuangan-harga">
    <div class="row">

        <div class="col-xs-12">

            <?php $form = 'id_keuangan_jenis' ?>
            <div class="form-group">
                <label class="control-label">Jenis Keuangan*</label>
                <select name="<?= $form ?>" id="select-jenis" class="form-control" required>
                    <option value="" selected disabled>PILIH JENIS</option>
                    <?php foreach($jenis_keuangan as $key => $item): ?>
                        <option value="<?= $item['id_keuangan_jenis'] ?>" <?= $type == 'edit' ? ($item['id_keuangan_jenis'] == $data['id_keuangan_jenis'] ? 'selected' : '') : '' ?>><?= $item['nm_jenis'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            
            <?php $form = 'jumlah' ?>
            <div class="form-group">
                <label class="control-label">Jumlah*</label>
                <input type="number" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Jumlah Harga" required>
            </div>

            <?php $form = 'tahun' ?>
            <div class="form-group">
                <label class="control-label">Tahun</label>
                <input type="text" name="<?= $form ?>" id="tahun-field" class="form-control yearpicker" value="<?= $type == 'edit' ? $data[$form] : date('Y') ?>" maxlength="4" placeholder="Masukkan Tahun Harga">
            </div>

            <div class="form-group">
                <button class="btn btn-block btn-success" id="btn-save-harga"><i class="fa fa-save"></i> Simpan</button>
            </div>
        
        </div>

    </div>
</form>

<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>
<script>
$(document).ready(function(){
    var base_url          = '<?= base_url() ?>';
    var type              = '<?= $type ?>';
    var formHargaKeuangan = $(document).find('#form-keuangan-harga');
    var saveBtn           = $(document).find('#btn-save-harga');
    var correspondTable   = $(document).find('#list-data-harga-keuangan');

    // when form-harga-keuangan submitted
    formHargaKeuangan.on('submit', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var $this = $(this);
        saveBtn.prop('disabled', true);
        saveBtn.html('<i class="fa fa-spin fa-spinner"></i> Menyimpan..');
        $.post($this.attr('action'), $this.serialize(), function(r){
            var result = $.parseJSON(r);

            Swal.fire({
                customClass: 'my-swal',
                type: result['status'],
                title: result['msg'],
                html: result['submsg']
            })

            if(type == 'tambah') {
                $this.trigger('reset');
            }

            correspondTable.DataTable().ajax.url(base_url+'admin/jenis-harga-keuangan/get-ajax-data').draw(false);

            saveBtn.prop('disabled', false);
            saveBtn.html('<i class="fa fa-save"></i> Simpan');
        });
    });
});
</script>