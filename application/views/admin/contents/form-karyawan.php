<?php
$id            = $type == 'edit' ? $data['id_karyawan'] : null;
$subcontroller = $this->uri->segment(2);
// var_dump($subcontroller);
?>

<form action="<?= base_url('admin/'.$subcontroller.'/save/'.$id) ?>" method="post" id="form-karyawan">
    <div class="row">

        <div class="col-xs-6">

            <?php $form = 'nm_karyawan' ?>
            <div class="form-group">
                <label class="control-label">Nama Karyawan*</label>
                <input type="text" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Nama Karyawan" autocomplete="off" required>
            </div>

            <?php $form = 'jenis_kelamin' ?>
            <div class="form-group">
                <label class="control-label">Jenis Kelamin*</label>
                <select name="<?= $form ?>" class="form-control" id="select-gender" required>
                    <option value="" selected disabled>PILIH JENIS KELAMIN</option>
                    <?php foreach($gender as $key => $value): ?>
                        <option value="<?= $key ?>" <?= $type == 'edit' ? ($key == $value ? 'selected' : '') : '' ?>><?= $value ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            
            <?php $form = 'tgl_lahir' ?>
            <div class="form-group">
                <label class="control-label">Tanggal Lahir*</label>
                <input type="text" name="<?= $form ?>" class="form-control datepicker" value="<?= ($type == 'edit' ? $data[$form] : '') ?>" placeholder="Masukkan Tanggal Lahir" autocomplete="off" required>
            </div>
            
            <?php $form = 'tgl_pengangkatan' ?>
            <div class="form-group">
                <label class="control-label">Tanggal Pengangkatan*</label>
                <input type="text" name="<?= $form ?>" class="form-control datepicker" value="<?= ($type == 'edit' ? $data[$form] : '') ?>" placeholder="Masukkan Tanggal Pengangkatan" autocomplete="off" required>
            </div>

            <?php $form = 'alamat' ?>
            <div class="form-group">
                <label class="control-label">Alamat Tempat Tinggal</label>
                <input type="text" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Alamat Tempat Tinggal" required>
            </div>
        
        </div>
        
        <div class="col-md-6">

            <?php $form = 'pendidikan' ?>
            <div class="form-group">
                <label class="control-label">Jenjang Pendidikan Terakhir*</label>
                <select name="<?= $form ?>" class="form-control" id="select-pendidikan" required>
                    <option value="" selected disabled>PILIH JENJANG PENDIDIKAN</option>
                    <?php foreach($pendidikan as $key => $value): ?>
                        <option value="<?= $value ?>" <?= $type == 'edit' ? ($value == $data[$form] ? 'selected' : '') : '' ?>><?= $value ?></option>
                    <?php endforeach ?>

                </select>
            </div>

            <?php $form = 'bidang_ilmu' ?>
            <div class="form-group">
                <label class="control-label">Bidang Keilmuan*</label>
                <input type="text" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Bidang Disiplin Ilmu" required>
            </div>
            
            <?php if($subcontroller == 'karyawan-non-medis'): ?>

                <?php $form = 'jabatan' ?>
                <div class="form-group">
                    <label class="control-label">Jabatan*</label>
                    <input type="text" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Jabatan Saat Ini">
                </div>

            <?php else: ?>

                <?php $form = 'id_bidang_kedokteran' ?>
                <div class="form-group">
                    <label class="control-label">Bidang Medis*</label>
                    <select name="<?= $form ?>" class="form-control" id="select-bidang" required>
                        <option value="" selected disabled>PILIH BIDANG</option>
                        <?php foreach($bidang as $key => $value): ?>
                            <option value="<?= $value['id_bidang_kedokteran'] ?>" <?= $type == 'edit' ? ($value['id_bidang_kedokteran'] == $data[$form] ? 'selected' : '') : '' ?>>(<?= $value['cabang']['nm_cabang_kedokteran'] ?>) <?= $value['nm_bidang_kedokteran'] ?></option>
                        <?php endforeach ?>

                    </select>
                </div>
                
            <?php endif ?>

            <?php $form = 'status' ?>
            <div class="form-group">
                <label class="control-label">Status Kekaryawanan*</label>
                <select name="<?= $form ?>" class="form-control" id="select-status" required>
                    <option value="" selected disabled>PILIH STATUS</option>
                    <?php foreach($status as $item): ?>
                        <option value="<?= $item ?>" <?= $type == 'edit' ? ($item == $data[$form] ? 'selected' : '') : '' ?>><?= $item ?></option>
                    <?php endforeach ?>
                </select>
            </div>

            <?php $form = 'email' ?>
            <div class="form-group">
                <label class="control-label">Email (opsional)</label>
                <input type="text" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Alamat Email" required>
            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">
                <button class="btn btn-block btn-success" id="btn-save-karyawan"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </div>

    </div>
</form>
<link rel="stylesheet" href="<?php echo theme_assets('css/select2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/select2.full.min.js', 'default'); ?>"></script>

<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>
<script>
$(document).ready(function(){
    var base_url           = '<?= base_url() ?>';
    var type               = '<?= $type ?>';
    var subcontroller      = '<?= $subcontroller ?>';
    var formKaryawan       = $(document).find('#form-karyawan');
    var saveBtn            = $(document).find('#btn-save-karyawan');
    var correspondTable_nm = $(document).find('#list-data-karyawan-nm');
    var correspondTable_m  = $(document).find('#list-data-karyawan-m');
    var selectBidang       = $(document).find('#select-bidang');

    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true
    });

    // add select2 search bar ti select box
    selectBidang.select2({width: '100%'});

    // when form-user submitted
    formKaryawan.on('submit', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var $this = $(this);
        saveBtn.prop('disabled', true);
        saveBtn.html('<i class="fa fa-spin fa-spinner"></i> Menyimpan..');
        $.post($this.attr('action'), $this.serialize(), function(r){
            var result = $.parseJSON(r);

            Swal.fire({
                customClass: 'my-swal',
                type: result['status'],
                title: result['msg'],
                html: result['submsg']
            })

            if(type == 'tambah') {
                $this.trigger('reset');
                selectBidang.val('').trigger('change');
            }

            if(subcontroller == 'karyawan-non-medis'){
                correspondTable_nm.DataTable().ajax.url(base_url+'admin/'+subcontroller+'/get-ajax-data').draw(false);
            }else{
                correspondTable_m.DataTable().ajax.url(base_url+'admin/'+subcontroller+'/get-ajax-data').draw(false);
            }

            saveBtn.prop('disabled', false);
            saveBtn.html('<i class="fa fa-save"></i> Simpan');
        });
    });
});
</script>