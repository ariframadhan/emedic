
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-success">
                <div class="box-header">
                    <a href="<?= base_url('admin/jenis-harga-keuangan/add-harga') ?>" class="btn btn-default modal-view" modal-size="modal-md" modal-title="Tambah Data Harga Keuangan"><i class="fa fa-plus"></i> Tambah Harga Keuangan</a>
                    <a href="<?= base_url('admin/jenis-harga-keuangan/jenis-keuangan') ?>" class="btn btn-default modal-view" modal-size="modal-md" modal-title="Jenis Keuangan"><i class="fa fa-money"></i> Jenis Keuangan</a>
                    <br><br>

                </div>
                <div class="box-body">
                    <table id="list-data-harga-keuangan" class="table table-bordered table-hover table-striped" width="100%">
                        <thead>
                            <tr>
                                <th class="no-sort text-center" width="10%">No</th>
                                <th width="45%">Jenis Keuangan</th>
                                <th class="text-center" width="15%">Tahun</th>
                                <th width="20%">Harga</th>
                                <th class="no-sort text-center" width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>
<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>

<script src="<?php echo theme_assets('js/custom.js', 'default'); ?>"></script>
<script>
$(document).ready(function(){
    var base_url = '<?= base_url() ?>';
    var table = $(document).find('#list-data-harga-keuangan');
    var ajax_url = base_url+'admin/jenis-harga-keuangan/get-ajax-data/';

    var dataTable = table.DataTable({
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var temp       = $('td:eq(0)', nRow).text();
            var temp       = temp.split('|');
            var numbering  = temp[0];
            var id         = temp[1];			
            
            $('td:eq(0)', nRow).html(numbering+'.');
            $('td:eq(0), td:eq(2), td:eq(3), td:eq(4)', nRow).addClass('text-center');
            $('td:eq(1)', nRow).addClass('text-left');
            
        },
        "responsive":false,
        "scrollX": true,
        "bAutoWidth": true,
        "iDisplayLength": 25,
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': ['no-sort']
            }
        ],
        "order": [[ 1, 'asc' ]],
        "language": {
            "search": "Pencarian",
            "lengthMenu": "Tampilkan _MENU_ Per Halaman",
            "paginate": {
                "first":      "Awal",
                "last":       "Akhir",
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            },
            "processing": "<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>",
            "zeroRecords": "Data tidak ditemukan",
            "loadingRecords": "<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>",
            "info": "Menampilkan _START_ - _END_ item dari total _TOTAL_ item",
        },
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": ajax_url,
    });

    $(document).on('click', '.btn-hapus', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var $this             = $(this);
        var id_keuangan_harga = $this.data('id-keuangan-harga');

        Swal.fire({
            customClass: 'my-swal',
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus harga keuangan secara permanen!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Lanjutkan',
            cancelButtonText: 'Batal'
        }).then(function(response){
            if(response.value){
                $this.prop('disabled', true); 
                $this.html('<i class="fa fa-spin fa-spinner"></i>');
                $.post(base_url+'admin/jenis-harga-keuangan/delete-harga/'+id_keuangan_harga, function(r){
                    var result = $.parseJSON(r);

                    var keyword = $('.dataTables_filter input').val();
                    if(result['status'] == 'success'){
                        dataTable.row($this.parents('tr')).remove().ajax.url(ajax_url).search(keyword).draw(false);
                    }
                    
                    Swal.fire({
                        customClass: 'my-swal',
                        type: result['status'],
                        title: result['msg'],
                        text: typeof result['submsg'] !== 'undefined' ? result['submsg'] : ''
                    });

                    $this.prop('disabled', false); 
                    $this.html('<i class="fa fa-trash-o"></i>');
                })
            }else{
                return;
            }
        }); 
    })

});
</script>
