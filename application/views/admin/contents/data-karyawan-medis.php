
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-success">
                <div class="box-header">
                    <a href="<?= base_url('admin/karyawan-medis/add') ?>" class="btn btn-default modal-view" modal-size="modal-lg" modal-title="Tambah Data Karyawan Medis"><i class="fa fa-plus"></i> Tambah Karyawan Medis</a>
                    <br><br>

                    <div class="row">

                        <div class="col-sm-12 col-md-3">
                            <div class="form-group">
                                <label class="control-label">Filter Status</label>
                                <select class="form-control filter-status">
                                    <option value="" selected disabled>PILIH STATUS</option>
                                    <?php foreach($status as $item): ?>
                                    <option value="<?= $item ?>"><?= str_replace('-',' ',$item) ?></option>
                                    <?php endforeach ?>
                                    <option value="all">SEMUA STATUS</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-sm-12 col-md-3">
                            <div class="form-group">
                                <label class="control-label">Filter Gender</label>
                                <select class="form-control filter-gender">
                                    <option value="" selected disabled>PILIH GENDER</option>
                                    <?php foreach($gender as $key => $item): ?>
                                    <option value="<?= $key ?>"><?= $item ?></option>
                                    <?php endforeach ?>
                                    <option value="all">SEMUA</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-sm-12 col-md-3">
                            <div class="form-group">
                                <label class="control-label">Filter Pendidikan</label>
                                <select class="form-control filter-pendidikan">
                                    <option value="" selected disabled>PILIH PENDIDIKAN</option>
                                    <?php foreach($pendidikan as $item): ?>
                                    <option value="<?= $item ?>"><?= $item ?></option>
                                    <?php endforeach ?>
                                    <option value="all">SEMUA</option>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="box-body">
                    <table id="list-data-karyawan-m" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th class="no-sort text-center">No</th>
                                <th class="text-center">NIK</th>
                                <th>Nama Karyawan</th>
                                <th class="text-center">Gender</th>
                                <th class="text-center">Bidang Kedokteran</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Tanggal Lahir</th>
                                <th class="no-sort text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>
<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>

<script src="<?php echo theme_assets('js/custom.js', 'default'); ?>"></script>
<script>
$(document).ready(function(){
    var base_url = '<?= base_url() ?>';
    var table = $(document).find('#list-data-karyawan-m');
    var ajax_url = base_url+'admin/karyawan-medis/get-ajax-data/';

    var dataTable = table.DataTable({
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var temp       = $('td:eq(0)', nRow).text();
            var temp       = temp.split('|');
            var numbering  = temp[0];
            var id         = temp[1];			
            
            $('td:eq(0)', nRow).html(numbering+'.');
            $('td:eq(0), td:eq(1), td:eq(3), td:eq(5), td:eq(6), td:eq(7)', nRow).addClass('text-center');
            $('td:eq(2), td:eq(4)', nRow).addClass('text-left');
            
        },
        "responsive":false,
        "scrollX": true,
        "bAutoWidth": false,
        "iDisplayLength": 25,
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': ['no-sort']
            }
        ],
        "order": [[ 1, 'asc' ]],
        "language": {
            "search": "Pencarian",
            "lengthMenu": "Tampilkan _MENU_ Per Halaman",
            "paginate": {
                "first":      "Awal",
                "last":       "Akhir",
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            },
            "processing": "<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>",
            "zeroRecords": "Data tidak ditemukan",
            "loadingRecords": "<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>",
            "info": "Menampilkan _START_ - _END_ item dari total _TOTAL_ item",
        },
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": ajax_url,
    });

    var scope_array = ['.filter-status','.filter-gender','.filter-pendidikan'];
    $.each(scope_array, function(key, val){
        onchange_filter($(val), scope_array, ajax_url, dataTable);
    });

    $(document).on('click', '.btn-hapus', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var $this       = $(this);
        var id_karyawan = $this.data('id-karyawan');

        Swal.fire({
            customClass: 'my-swal',
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus karyawan secara permanen!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Lanjutkan',
            cancelButtonText: 'Batal'
        }).then(function(response){
            if(response.value){
                $this.prop('disabled', true); 
                $this.html('<i class="fa fa-spin fa-spinner"></i>');
                $.post(base_url+'admin/karyawan-medis/delete/'+id_karyawan, function(r){
                    var result = $.parseJSON(r);

                    var keyword = $('.dataTables_filter input').val();
                    if(result['status'] == 'success'){
                        dataTable.row($this.parents('tr')).remove().ajax.url(ajax_url).search(keyword).draw(false);
                    }
                    
                    Swal.fire({
                        customClass: 'my-swal',
                        type: result['status'],
                        title: result['msg'],
                        text: typeof result['submsg'] !== 'undefined' ? result['submsg'] : ''
                    });

                    $this.prop('disabled', false); 
                    $this.html('<i class="fa fa-trash-o"></i>');
                })
            }else{
                return;
            }
        }); 
    })

});
</script>
