<?php
$id = $type == 'edit' ? $data['id_pasien'] : null;
?>

<form action="<?= base_url('admin/pasien/save/'.$id) ?>" method="post" id="form-pasien">
    <div class="row">

        <div class="col-xs-12">

            <?php $form = 'nm_pasien' ?>
            <div class="form-group">
                <label class="control-label">Nama Pasien*</label>
                <input type="text" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Nama Pasien" required>
            </div>
            
            <?php $form = 'tgl_lahir' ?>
            <div class="form-group">
                <label class="control-label">Tanggal Lahir*</label>
                <input type="text" name="<?= $form ?>" class="form-control datepicker" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Tanggal Lahir" required autocomplete="off">
            </div>

            <?php $form = 'kategori_penyakit' ?>
            <div class="form-group">
                <label class="control-label">Kategori Penyakit*</label>
                <select name="<?= $form ?>" class="form-control" id="select-kategori-penyakit" required>
                    <option value="" selected disabled>PILIH KATEGORI</option>
                    <?php foreach($kategori as $item): ?>
                        <option value="<?= $item ?>" <?= $type == 'edit' ? ($item == $data[$form] ? 'selected' : '') : '' ?>><?= $item ?></option>
                    <?php endforeach ?>
                </select>
            </div>

            <?php $form = 'no_telp' ?>
            <div class="form-group">
                <label class="control-label">Nomor Telepon</label>
                <input type="number" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Contoh : 085123xxxx">
            </div>

            <?php $form = 'alamat' ?>
            <div class="form-group">
                <label class="control-label">Alamat</label>
                <textarea name="<?= $form ?>" id="alamat-field" cols="30" rows="10" class="form-control" placeholder="Alamat (Opsional)"><?= $type == 'edit' ? $data[$form] : '' ?></textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-block btn-success" id="btn-save-pasien"><i class="fa fa-save"></i> Simpan</button>
            </div>
        
        </div>

    </div>
</form>

<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>
<script>
$(document).ready(function(){
    var base_url        = '<?= base_url() ?>';
    var type            = '<?= $type ?>';
    var formPasien      = $(document).find('#form-pasien');
    var saveBtn         = $(document).find('#btn-save-pasien');
    var correspondTable = $(document).find('#list-data-pasien');

    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true
    });

    // when form-pasien submitted
    formPasien.on('submit', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var $this = $(this);
        saveBtn.prop('disabled', true);
        saveBtn.html('<i class="fa fa-spin fa-spinner"></i> Menyimpan..');
        $.post($this.attr('action'), $this.serialize(), function(r){
            var result = $.parseJSON(r);

            Swal.fire({
                customClass: 'my-swal',
                type: result['status'],
                title: result['msg'],
                html: result['submsg']
            })

            if(type == 'tambah') {
                $this.trigger('reset');
            }

            correspondTable.DataTable().ajax.url(base_url+'admin/pasien/get-ajax-data').draw(false);

            saveBtn.prop('disabled', false);
            saveBtn.html('<i class="fa fa-save"></i> Simpan');
        });
    });
});
</script>