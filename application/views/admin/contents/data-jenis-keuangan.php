<div class="box no-border">
    <div class="box-body">
        <div class="table-responsive" id="table-jenis-wrapper">
            <table class="table table-bordered table-striped table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th class="text-center">No.</th>
                        <th style="width:60%">Jenis Keuangan</th>
                        <th style="width:30%" class="text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($data)): ?>
                        <?php $i=1; foreach($data as $key => $value): ?>
                            <tr>
                                <td class="text-center"><?= $i ?></td>
                                <td data-id-keuangan-jenis="<?= $value['id_keuangan_jenis'] ?>" class="name-field"><?= $value['nm_jenis'] ?></td>
                                <td data-id-keuangan-jenis="<?= $value['id_keuangan_jenis'] ?>" class="text-center button-field">
                                    <button class="btn btn-default btn-edit-jenis" data-id-keuangan-jenis="<?= $value['id_keuangan_jenis'] ?>"><i class="fa fa-edit"></i></button>
                                    <button class="btn btn-default btn-delete-jenis" data-id-keuangan-jenis="<?= $value['id_keuangan_jenis'] ?>"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        <?php $i++; endforeach ?>
                            
                            <tr>
                                <td></td>
                                <td>
                                    <form action="<?= base_url('admin/jenis-harga-keuangan/save-jenis') ?>" id="form-jenis-baru">
                                        <input type="text" class="form-control new-jenis" placeholder="Masukkan Nama Jenis" required>
                                    </form>
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-default btn-add-jenis"><i class="fa fa-plus"></i> Tambah</button>
                                </td>
                            </tr>
                            
                    <?php else: ?>
                        <tr>
                            <td colspan="3" class="text-center">Belum Ada Data</td>
                        </tr>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

    var base_url = '<?= base_url() ?>';

    $(document).on('click', '.btn-add-jenis', function(){
        console.log('clicked');
        $(document).find('#form-jenis-baru').submit();
    });

    $(document).on('submit', '#form-jenis-baru', function(e){
        e.preventDefault();
        var post_url = $(this).attr('action');
        if(!$('.new-jenis').val()){
            alert('Kolom Nama Jenis Belum Diisi');
            return;
        }else{
            var nm_jenis = $('.new-jenis').val();
            $('.btn-add-jenis').prop('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
            $.post(post_url, {nm_jenis, nm_jenis}, function(r){
                var result = $.parseJSON(r);
                if(result['status'] == 'success'){
                    $('#table-jenis-wrapper').load(base_url+'admin/jenis-harga-keuangan/jenis-keuangan #table-jenis-wrapper');
                }else{
                    alert('Gagal Tambah Jenis Keuangan Baru');
                }
            });
        }
    });

    $(document).on('click', '.btn-edit-jenis', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var id_keuangan_jenis  = $(this).data('id-keuangan-jenis');
        var row_input          = $(document).find('.name-field[data-id-keuangan-jenis='+id_keuangan_jenis+']');
        var row_button         = $(document).find('.button-field[data-id-keuangan-jenis='+id_keuangan_jenis+']');
        var row_input_contents = row_input.html().toString();
        var input = '<input type="text" class="form-control edit-field" data-id-keuangan-jenis='+id_keuangan_jenis+' value="'+row_input_contents+'">';
        var btns  = '<button class="btn btn-default btn-save-edit" data-id-keuangan-jenis='+id_keuangan_jenis+'><i class="fa fa-check"></i></button> <button class="btn btn-default btn-cancel-edit" data-id-keuangan-jenis='+id_keuangan_jenis+'><i class="fa fa-close"></i></button>';
        row_input.html(input);
        row_button.html(btns);
    });

    $(document).on('click', '.btn-delete-jenis', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var id_keuangan_jenis  = $(this).data('id-keuangan-jenis');
        if(confirm('Apakah anda yakin akan menghapus data ini?')){
            $(this).prop('disabled', true).html('<i class="fa fa-spin fa-spinner"></i>');
            $.post(base_url+'admin/jenis-harga-keuangan/delete-jenis/'+id_keuangan_jenis, {}, function(r){
                var result = $.parseJSON(r);
                if(result['status'] == 'success'){
                    $('#table-jenis-wrapper').load(base_url+'admin/jenis-harga-keuangan/jenis-keuangan #table-jenis-wrapper');
                }else{
                    alert('Gagal Hapus Jenis Keuangan');
                }
            });
        }else{
            return;
        }
    });

    $(document).on('click', '.btn-cancel-edit', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var id_keuangan_jenis  = $(this).data('id-keuangan-jenis');
        var row_input          = $(document).find('.name-field[data-id-keuangan-jenis='+id_keuangan_jenis+']');
        var row_button         = $(document).find('.button-field[data-id-keuangan-jenis='+id_keuangan_jenis+']');
        var row_input_contents = row_input.find('input').val().toString();

        var btns  = '<button class="btn btn-default btn-edit-jenis" data-id-keuangan-jenis='+id_keuangan_jenis+'><i class="fa fa-edit"></i></button> <button class="btn btn-default btn-delete-jenis" data-id-keuangan-jenis='+id_keuangan_jenis+'><i class="fa fa-trash"></i></button>';
        row_input.html(row_input_contents);
        row_button.html(btns);
    });
    
    $(document).on('click', '.btn-save-edit', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var id_keuangan_jenis  = $(this).data('id-keuangan-jenis');
        var row_input          = $(document).find('.name-field[data-id-keuangan-jenis='+id_keuangan_jenis+']');
        var input_values       = row_input.find('input').val().toString();
        $.post(base_url+'admin/jenis-harga-keuangan/save-jenis/'+id_keuangan_jenis, {nm_jenis: input_values}, function(r){
            var result = $.parseJSON(r);
            if(result['status'] == 'success'){
                $('#table-jenis-wrapper').load(base_url+'admin/jenis-harga-keuangan/jenis-keuangan #table-jenis-wrapper');
            }else{
                alert('Gagal Edit Jenis Keuangan');
            }
        })
    });

});
</script>