<?php //var_dump($data) ?>
<div class="box no-border">
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-stripped table-hover table-bordered">
                <tr>
                    <th style="width:40%">Nomor Induk Karyawan</th>
                    <td><?= $data['nik'] ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Nama Karyawan</th>
                    <td><?= $data['nm_karyawan'] ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Pendidikan Terakhir</th>
                    <td><?= $data['pendidikan'] ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Bidang Disiplin Ilmu</th>
                    <td><?= $data['bidang_ilmu'] ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Tanggal Lahir</th>
                    <td><?= tgl_indo($data['tgl_lahir']).' ('.get_umur($data['tgl_lahir']).' Tahun)' ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Tanggal Pengangkatan</th>
                    <td><?= tgl_indo($data['tgl_pengangkatan']) ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Jabatan Saat Ini</th>
                    <td><?= $data['jabatan'] ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Jenis Karyawan</th>
                    <td><?= str_replace('-',' ',$data['jenis_karyawan']) ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Bidang Medis</th>
                    <td><?= !empty($data['cabang']) ? '('.$data['cabang']['nm_cabang_kedokteran'].') '.$data['bidang']['nm_bidang_kedokteran'] : '' ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Status</th>
                    <td><?= $data['status'] ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Email</th>
                    <td><?= $data['email'] ?></td>
                </tr>
                <tr>
                    <th style="width:40%">Alamat</th>
                    <td><?= $data['alamat'] ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>