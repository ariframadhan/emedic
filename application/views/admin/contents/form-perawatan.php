<?php
$id = $type == 'edit' ? $data['id_perawatan'] : null;
?>

<form action="<?= base_url('admin/perawatan/save/'.$id) ?>" method="post" id="form-perawatan">
    <div class="row">

        <div class="col-xs-12">

            <!-- //id_pasien -->
            <?php $form = 'id_pasien' ?>
            <div class="form-group">
                <label class="control-label">Pasien*</label>
                <select name="<?= $form ?>" class="form-control" id="select-pasien" required>
                    <?php if($type == 'edit'): ?>
                        <option value="<?= $data['id_pasien'] ?>"><?= $data['pasien']['nm_pasien'] ?></option>
                    <?php endif ?>
                </select>
            </div>

            <!-- //id_karyawan -->
            <?php $form = 'id_karyawan' ?>
            <div class="form-group">
                <label class="control-label">Perawat / Dokter*</label>
                <select name="<?= $form ?>" class="form-control" id="select-dokter-perawatan" required>
                    <?php if($type == 'edit'): ?>
                        <option value="<?= $data['id_karyawan'] ?>"><?= $data['karyawan']['nm_karyawan'] ?></option>
                    <?php endif ?>
                </select>
            </div>
            
            <?php $form = 'tgl_dirawat' ?>
            <div class="form-group">
                <label class="control-label">Tanggal Dirawat*</label>
                <input type="text" name="<?= $form ?>" class="form-control datepicker" value="<?= $type == 'edit' ? $data[$form] : date('Y-m-d h:i:s') ?>" placeholder="Masukkan Tanggal Mulai Dirawat" required autocomplete="off">
            </div>

            <?php $form = 'status' ?>
            <div class="form-group">
                <label class="control-label">Status Perawatan*</label>
                <select name="<?= $form ?>" class="form-control" id="select-status-perawatan" required>
                    <option value="" selected disabled>PILIH STATUS</option>
                    <?php $status = array('DALAM-ANTRIAN', 'SEDANG-DITANGANI') ?>
                    <?php foreach($status as $item): ?>
                        <option value="<?= $item ?>" <?= $type == 'edit' ? ($item == $data[$form] ? 'selected' : '') : '' ?>><?= $item ?></option>
                    <?php endforeach ?>
                </select>
            </div>

            <?php $form = 'keterangan' ?>
            <div class="form-group">
                <label class="control-label">Keterangan</label>
                <textarea name="<?= $form ?>" id="keterangan-perawatan-field" cols="30" rows="10" class="form-control" placeholder="Keterangan (Opsional)"><?= $type == 'edit' ? $data[$form] : '' ?></textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-block btn-success" id="btn-save-perawatan"><i class="fa fa-save"></i> Simpan</button>
            </div>
        
        </div>

    </div>
</form>

<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>

<link rel="stylesheet" href="<?php echo theme_assets('css/select2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/select2.full.min.js', 'default'); ?>"></script>

<script>
$(document).ready(function(){
    var base_url        = '<?= base_url() ?>';
    var type            = '<?= $type ?>';
    var formPerawatan   = $(document).find('#form-perawatan');
    var saveBtn         = $(document).find('#btn-save-perawatan');
    var correspondTable = $(document).find('#list-data-perawatan');

    var selectPasien    = $(document).find('#select-pasien');
    var selectPerawatan = $(document).find('#select-dokter-perawatan');

    $('.datepicker').datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss"
    });

    // Select2 Ajax Pasien
    selectPasien.select2({
        placeholder: 'Masukkan Nama Pasien',
        width: '100%',
        // dropdownParent: $('#pasien-segment'),
        language: {
            noResults: function (params) {
                return "Tidak ada Data yang cocok dengan Keyword.";
            },
            searching: function (params) {
                return "Mencari...";
            },
            inputTooShort: function (params) {
                var x = params.minimum - params.input.length;
                return "Masukkan "+ x +" karakter lagi";
            }
        },
        minimumInputLength: 2,
        ajax: {
            url: base_url+'admin/perawatan/get-pasien/',
            dataType: 'json',
            delay: 750,
            data: function (params) {
                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        if (item.id_pasien !== '') {
                            return {
                                id  : item.id_pasien,
                                text: item.nm_pasien,
                            };
                        } else {
                            return false;
                        }
                    })
                };
            },
            cache: true
        }
    });
    
    // Select2 Ajax Karyawan Medis
    selectPerawatan.select2({
        placeholder: 'Masukkan Nama Karyawan',
        width: '100%',
        language: {
            noResults: function (params) {
                return "Tidak ada Data yang cocok dengan Keyword.";
            },
            searching: function (params) {
                return "Mencari...";
            },
            inputTooShort: function (params) {
                var x = params.minimum - params.input.length;
                return "Masukkan "+ x +" karakter lagi";
            }
        },
        minimumInputLength: 2,
        ajax: {
            url: base_url+'admin/perawatan/get-karyawan-medis/',
            dataType: 'json',
            delay: 750,
            data: function (params) {
                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        if (item.id_karyawan !== '') {
                            return {
                                id  : item.id_karyawan,
                                text: item.nm_karyawan,
                            };
                        } else {
                            return false;
                        }
                    })
                };
            },
            cache: true
        }
    });

    // when form-pasien submitted
    formPerawatan.on('submit', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var $this = $(this);
        saveBtn.prop('disabled', true);
        saveBtn.html('<i class="fa fa-spin fa-spinner"></i> Menyimpan..');
        $.post($this.attr('action'), $this.serialize(), function(r){
            var result = $.parseJSON(r);

            Swal.fire({
                customClass: 'my-swal',
                type: result['status'],
                title: result['msg'],
                html: result['submsg']
            })

            if(type == 'tambah') {
                $this.trigger('reset');
            }

            correspondTable.DataTable().ajax.url(base_url+'admin/perawatan/get-ajax-data').draw(false);

            saveBtn.prop('disabled', false);
            saveBtn.html('<i class="fa fa-save"></i> Simpan');
        });
    });
});
</script>