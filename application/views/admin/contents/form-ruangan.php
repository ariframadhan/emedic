<?php
$id = $type == 'edit' ? $data['id_ruangan'] : null;
?>

<form action="<?= base_url('admin/ruangan/save/'.$id) ?>" method="post" id="form-ruangan">
    <div class="row">

        <div class="col-xs-12">

            <?php $form = 'nm_ruangan' ?>
            <div class="form-group">
                <label class="control-label">Nama Ruangan*</label>
                <input type="text" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Nama Ruangan" required>
            </div>
            
            <?php $form = 'kapasitas_pasien' ?>
            <div class="form-group">
                <label class="control-label">Kapasitas Pasien*</label>
                <input type="number" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Jumlah Kapasitas Pasien" required>
            </div>

            <?php $form = 'keterangan' ?>
            <div class="form-group">
                <label class="control-label">Keterangan</label>
                <textarea name="<?= $form ?>" id="keterangan-field" cols="30" rows="10" class="form-control" placeholder="Keterangan (Opsional)"><?= $type == 'edit' ? $data[$form] : '' ?></textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-block btn-success" id="btn-save-ruangan"><i class="fa fa-save"></i> Simpan</button>
            </div>
        
        </div>

    </div>
</form>

<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>
<script>
$(document).ready(function(){
    var base_url        = '<?= base_url() ?>';
    var type            = '<?= $type ?>';
    var formRuangan     = $(document).find('#form-ruangan');
    var saveBtn         = $(document).find('#btn-save-ruangan');
    var correspondTable = $(document).find('#list-data-ruangan');

    // when form-ruangan submitted
    formRuangan.on('submit', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var $this = $(this);
        saveBtn.prop('disabled', true);
        saveBtn.html('<i class="fa fa-spin fa-spinner"></i> Menyimpan..');
        $.post($this.attr('action'), $this.serialize(), function(r){
            var result = $.parseJSON(r);

            Swal.fire({
                customClass: 'my-swal',
                type: result['status'],
                title: result['msg'],
                html: result['submsg']
            })

            if(type == 'tambah') {
                $this.trigger('reset');
            }

            correspondTable.DataTable().ajax.url(base_url+'admin/ruangan/get-ajax-data').draw(false);

            saveBtn.prop('disabled', false);
            saveBtn.html('<i class="fa fa-save"></i> Simpan');
        });
    });
});
</script>