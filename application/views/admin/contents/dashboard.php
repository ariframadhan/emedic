
<section class="content">
    <div class="row">

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?= $total_karyawan ?></h3>

              <p>Karyawan dan Dokter</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-md"></i>
            </div>
            <a href="<?php echo base_url('admin/karyawan-medis'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?= $total_pasien ?></h3>

              <p>Pasien</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="<?php echo base_url('admin/pasien'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?= $total_perawatan ?></h3>

              <p>Aktifitas Perawatan</p>
            </div>
            <div class="icon">
              <i class="fa fa-heartbeat"></i>
            </div>
            <a href="<?php echo base_url('admin/perawatan'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>726k</h3>

              <p>Transaksi Keuangan</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="<?php echo base_url('admin/keuangan-transaksi'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">
            <div class="box box-success">
                <div class="box-header">
                    <h3>Rekapitulasi Medis 2020</h3>
                </div>
                <div class="box-body">
                    <div id="myfirstchart" style="height: 40rem;"></div>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- Morris chart -->
<link rel="stylesheet" href="<?= theme_assets('bower_components/morris.js/morris.css','admin')?>">
<!-- Morris.js charts -->
<script src="<?= theme_assets('bower_components/raphael/raphael.min.js','admin')?>"></script>
<script src="<?= theme_assets('bower_components/morris.js/morris.min.js','admin')?>"></script>

<script>
$(document).ready(function(){

    var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
    new Morris.Area({
        element: 'myfirstchart',
        data: [
            { m: '2020-01', sembuh: 20, dirawat: 10, meninggal: 5 },
            { m: '2020-02', sembuh: 40, dirawat: 20, meninggal: 10 },
            { m: '2020-03', sembuh: 60, dirawat: 30, meninggal: 15 },
            { m: '2020-04', sembuh: 80, dirawat: 40, meninggal: 20 },
            { m: '2020-05', sembuh: 80, dirawat: 40, meninggal: 20 },
            { m: '2020-06', sembuh: 100, dirawat: 50, meninggal: 25 },
            { m: '2020-07', sembuh: 40, dirawat: 20, meninggal: 10 },
            { m: '2020-08', sembuh: 100, dirawat: 50, meninggal: 25 },
            { m: '2020-09', sembuh: 160, dirawat: 80, meninggal: 40 },
            { m: '2020-10', sembuh: 100, dirawat: 50, meninggal: 25 },
            { m: '2020-11', sembuh: 80, dirawat: 40, meninggal: 20 },
            { m: '2020-12', sembuh: 60, dirawat: 30, meninggal: 15 },
        ],
        fillOpacity: 0.5,
        hideHover: 'auto',
        xkey: 'm',
        ykeys: ['sembuh', 'dirawat', 'meninggal'],
        labels: ['Pasien Sembuh','Pasien Dirawat', 'Pasien Meninggal'],
        behaveLikeLine: true,
        resize: true,
        pointStrokeColors: ['white'],
        lineColors: ['green', 'blue', 'grey'],
        xLabelFormat: function(x){
            var month = months[x.getMonth()];
            return month;
        },
        dateFormat: function(x) {
            var month = months[new Date(x).getMonth()];
            return month;
        },
        });

});
</script>
