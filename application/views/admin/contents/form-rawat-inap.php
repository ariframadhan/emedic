<form action="<?= base_url('admin/rawat-inap/save/'.$data['id_rawat_inap']) ?>" method="post" id="form-rawat-inap">
    <div class="row">

        <div class="col-xs-12">
            
            <div class="form-group">
                <label class="control-label">Pasien Perawatan</label>
                <input type="text" class="form-control" value="<?= '(Kode : '.$data['perawatan']['id_perawatan'].') '.$data['pasien']['nm_pasien'] ?>" readonly autocomplete="off">
            </div>

            <div class="form-group">
                <label class="control-label">Tanggal Dirawat</label>
                <input type="text" class="form-control" value="<?= $data['perawatan']['tgl_dirawat'] ?>" readonly autocomplete="off">
            </div>

            <!-- //id_ruangan -->
            <?php $form = 'id_ruangan' ?>
            <div class="form-group">
                <label class="control-label">Ruangan Perawatan*</label>
                <select name="<?= $form ?>" class="form-control" id="select-ruangan" required>
                    <?php if(!empty($data['ruangan'])): ?>
                        <option value="<?= $data['ruangan']['id_ruangan'] ?>"><?= $data['ruangan']['nm_ruangan'] ?></option>
                    <?php endif ?>
                </select>
            </div>

            <div class="form-group">
                <button class="btn btn-block btn-success" id="btn-save-rawat-inap"><i class="fa fa-save"></i> Simpan</button>
            </div>
        
        </div>

    </div>
</form>

<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>

<link rel="stylesheet" href="<?php echo theme_assets('css/select2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/select2.full.min.js', 'default'); ?>"></script>

<script>
$(document).ready(function(){
    var base_url        = '<?= base_url() ?>';
    var formRawatInap   = $(document).find('#form-rawat-inap');
    var saveBtn         = $(document).find('#btn-save-rawat-inap');
    var correspondTable = $(document).find('#list-data-rawat-inap');

    var selectRuangan   = $(document).find('#select-ruangan');

    // Select2 Ajax Ruangan
    selectRuangan.select2({
        placeholder: 'Masukkan Nama Ruangan',
        width: '100%',
        // dropdownParent: $('#pasien-segment'),
        language: {
            noResults: function (params) {
                return "Tidak ada Data yang cocok dengan Keyword atau Ruangan yang anda maksud sudah penuh.";
            },
            searching: function (params) {
                return "Mencari...";
            },
            inputTooShort: function (params) {
                var x = params.minimum - params.input.length;
                return "Masukkan "+ x +" karakter lagi";
            }
        },
        minimumInputLength: 2,
        ajax: {
            url: base_url+'admin/rawat-inap/get-ruangan-tersedia/',
            dataType: 'json',
            delay: 750,
            data: function (params) {
                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        if (item.id_ruangan !== '') {
                            return {
                                id  : item.id_ruangan,
                                text: item.nm_ruangan,
                            };
                        } else {
                            return false;
                        }
                    })
                };
            },
            cache: true
        }
    });

    // when form-rawat-inap submitted
    formRawatInap.on('submit', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var $this = $(this);
        saveBtn.prop('disabled', true);
        saveBtn.html('<i class="fa fa-spin fa-spinner"></i> Menyimpan..');
        $.post($this.attr('action'), $this.serialize(), function(r){
            var result = $.parseJSON(r);

            Swal.fire({
                customClass: 'my-swal',
                type: result['status'],
                title: result['msg'],
                html: result['submsg']
            });

            $('#rawat-inap-notif').load(base_url+'admin/rawat-inap #rawat-inap-notif', function(){
                correspondTable.DataTable().ajax.url(base_url+'admin/rawat-inap/get-ajax-data').draw(false);
            });


            saveBtn.prop('disabled', false);
            saveBtn.html('<i class="fa fa-save"></i> Simpan');
        });
    });
});
</script>