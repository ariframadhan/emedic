<?php
$is_edit_akun = false;
if($type == 'edit-akun'){
    $is_edit_akun = true;
    $type = 'edit';
}

$id = $type == 'edit' ? $data['id_user'] : null;
?>

<form action="<?= base_url('admin/user/save/'.$id) ?>" method="post" id="form-user">
    <div class="row">

        <div class="col-xs-12">

            <?php $form = 'nik' ?>
            <div class="form-group">
                <label class="control-label">Karyawan*</label>
                <select name="<?= $form ?>" class="form-control" id="select-karyawan" required>
                    <option value="" selected disabled>PILIH KARYAWAN</option>
                    <?php foreach($karyawan as $key => $value): ?>
                        <option value="<?= $value['nik'] ?>" <?= $type == 'edit' ? ($value['nik'] == $data['nik'] ? 'selected' : '') : '' ?>>(<?= $value['nik'] ?>) <?= $value['nm_karyawan'] ?></option>
                    <?php endforeach ?>

                </select>
            </div>

            <?php $form = 'username' ?>
            <div class="form-group">
                <label class="control-label">Username*</label>
                <input type="text" name="<?= $form ?>" class="form-control" value="<?= $type == 'edit' ? $data[$form] : '' ?>" placeholder="Masukkan Username" required>
            </div>
            
            <?php $form = 'password' ?>
            <div class="form-group">
                <label class="control-label">Password*</label>
                <input type="password" name="<?= $form ?>" class="form-control" value="" placeholder="Masukkan Password" <?= $type == 'tambah' ? 'required' : '' ?>>
            </div>

            <?php if(!$is_edit_akun): ?>
            <?php $form = 'level' ?>
            <div class="form-group">
                <label class="control-label">Karyawan*</label>
                <select name="<?= $form ?>" class="form-control" id="select-level" required>
                    <option value="" selected disabled>PILIH LEVEL</option>
                    <?php foreach($level as $item): ?>
                        <option value="<?= $item ?>" <?= $type == 'edit' ? ($item == $data[$form] ? 'selected' : '') : '' ?>><?= $item ?></option>
                    <?php endforeach ?>

                </select>
            </div>
            <?php endif ?>

            <div class="form-group">
                <button class="btn btn-block btn-success" id="btn-save-user"><i class="fa fa-save"></i> Simpan</button>
            </div>
        
        </div>

    </div>
</form>
<link rel="stylesheet" href="<?php echo theme_assets('css/select2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/select2.full.min.js', 'default'); ?>"></script>

<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>
<script>
$(document).ready(function(){
    var base_url        = '<?= base_url() ?>';
    var type            = '<?= $type ?>';
    var selectKaryawan  = $(document).find('#select-karyawan');
    var formUser        = $(document).find('#form-user');
    var saveBtn         = $(document).find('#btn-save-user');
    var correspondTable = $(document).find('#list-data');

    // add select2 search bar ti select box
    selectKaryawan.select2({width: '100%'});

    // when form-user submitted
    formUser.on('submit', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var $this = $(this);
        saveBtn.prop('disabled', true);
        saveBtn.html('<i class="fa fa-spin fa-spinner"></i> Menyimpan..');
        $.post($this.attr('action'), $this.serialize(), function(r){
            var result = $.parseJSON(r);

            Swal.fire({
                customClass: 'my-swal',
                type: result['status'],
                title: result['msg'],
                html: result['submsg']
            })

            if(type == 'tambah') {
                $this.trigger('reset');
                selectKaryawan.val('').trigger('change');
            }

            correspondTable.DataTable().ajax.url(base_url+'admin/user/get-ajax-data').draw(false);

            saveBtn.prop('disabled', false);
            saveBtn.html('<i class="fa fa-save"></i> Simpan');
        });
    });
});
</script>