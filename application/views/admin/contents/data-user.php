
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-success">
                <div class="box-header">
                    <a href="<?= base_url('admin/user/add') ?>" class="btn btn-default modal-view" modal-size="modal-md" modal-title="Tambah Data User"><i class="fa fa-plus"></i> Tambah User</a>
                    <br><br>

                    <div class="row">

                        <div class="col-sm-12 col-md-3">
                            <div class="form-group">
                                <label class="control-label">Filter Level</label>
                                <select class="form-control filter-level">
                                    <option value="" selected disabled>PILIH LEVEL</option>
                                    <?php foreach($level_user as $item): ?>
                                    <option value="<?= $item ?>"><?= str_replace('-',' ',$item) ?></option>
                                    <?php endforeach ?>
                                    <option value="all">SEMUA LEVEL</option>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="box-body">
                    <table id="list-data" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th class="no-sort">No</th>
                                <th>Username</th>
                                <th>Nama Karyawan</th>
                                <th>Email</th>
                                <th>Level</th>
                                <th>Terdaftar Pada</th>
                                <th class="no-sort">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>
<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>

<script src="<?php echo theme_assets('js/custom.js', 'default'); ?>"></script>
<script>
$(document).ready(function(){
    var base_url = '<?= base_url() ?>';
    var table = $(document).find('#list-data');
    var ajax_url = base_url+'admin/user/get-ajax-data/';

    var dataTable = table.DataTable({
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var temp       = $('td:eq(0)', nRow).text();
            var temp       = temp.split('|');
            var numbering  = temp[0];
            var id         = temp[1];			
            
            $('td:eq(0)', nRow).html(numbering+'.');
            $('td:eq(0), td:eq(1), td:eq(4), td:eq(6)', nRow).addClass('text-center');
            $('td:eq(2), td:eq(3), td:eq(5)', nRow).addClass('text-left');
            
        },
        "responsive":false,
        "scrollX": true,
        "bAutoWidth": false,
        "iDisplayLength": 25,
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': ['no-sort']
            }
        ],
        "order": [[ 1, 'asc' ]],
        "language": {
            "search": "Pencarian",
            "lengthMenu": "Tampilkan _MENU_ Per Halaman",
            "paginate": {
                "first":      "Awal",
                "last":       "Akhir",
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            },
            "processing": "<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>",
            "zeroRecords": "Data tidak ditemukan",
            "loadingRecords": "<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>",
            "info": "Menampilkan _START_ - _END_ item dari total _TOTAL_ item",
        },
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": ajax_url,
    });

    var scope_array = ['.filter-level'];
    $.each(scope_array, function(key, val){
        onchange_filter($(val), scope_array, ajax_url, dataTable);
    });

    $(document).on('click', '.btn-hapus', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var $this   = $(this);
        var id_user = $this.data('id-user');

        Swal.fire({
            customClass: 'my-swal',
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus user secara permanen!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Lanjutkan',
            cancelButtonText: 'Batal'
        }).then(function(response){
            if(response.value){
                $this.prop('disabled', true); 
                $this.html('<i class="fa fa-spin fa-spinner"></i>');
                $.post(base_url+'admin/user/delete/'+id_user, function(r){
                    var result = $.parseJSON(r);

                    var keyword = $('.dataTables_filter input').val();
                    if(result['status'] == 'success'){
                        dataTable.row($this.parents('tr')).remove().ajax.url(ajax_url).search(keyword).draw(false);
                    }
                    
                    Swal.fire({
                        customClass: 'my-swal',
                        type: result['status'],
                        title: result['msg'],
                        text: typeof result['submsg'] !== 'undefined' ? result['submsg'] : ''
                    });

                    $this.prop('disabled', false); 
                    $this.html('<i class="fa fa-trash-o"></i>');
                })
            }else{
                return;
            }
        }); 
    })

});
</script>
