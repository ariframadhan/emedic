<div class="row">

    <div class="col-sm-12 col-md-4">
        <div class="box box-success">
            <div class="box-header text-center">
                <h4>Tambah Data Transaksi Keuangan</h4>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-8">
        <div class="box box-success">
            <div class="box-header">
                <h4>Riwayat Transaksi Keuangan</h4>
                <br>

                <div class="row">

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Filter Metode Pembayaran</label>
                            <select id="filter-metode" class="form-control">
                                <option value="" selected disabled>PILIH METODE</option>
                                <?php foreach($metode_pembayaran as $item): ?>
                                    <option value="<?= $item ?>"><?= $item ?></option>
                                <?php endforeach ?>
                                    <option value="all">SEMUA METODE</option>
                            </select>
                        </div>
                    </div>

                </div>

            </div>
            <div class="box-body">
                <table id="list-data-keuangan" class="table table-bordered table-hover table-striped" width="100%">
                    <thead>
                        <tr>
                            <th class="no-sort">No</th>
                            <th>Nomor Transaksi</th>
                            <th>Nama Pasien</th>
                            <th>Tanggal Transaksi</th>
                            <th>Metode Pembayaran</th>
                            <th>Total Transaksi</th>
                            <th>Admin</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<link rel="stylesheet" href="<?php echo theme_assets('css/sweetalert2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/sweetalert2.min.js', 'default'); ?>"></script>

<link rel="stylesheet" href="<?php echo theme_assets('css/select2.min.css', 'default'); ?>">
<script src="<?php echo theme_assets('js/select2.full.min.js', 'default'); ?>"></script>

<script src="<?php echo theme_assets('js/custom.js', 'default'); ?>"></script>
<script>
$(document).ready(function(){
    var base_url = '<?= base_url() ?>';
    var table = $(document).find('#list-data-keuangan');
    var ajax_url = base_url+'admin/keuangan/get-ajax-data/';

    var dataTable = table.DataTable({
        "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var temp       = $('td:eq(0)', nRow).text();
            var temp       = temp.split('|');
            var numbering  = temp[0];
            var id         = temp[1];			
            
            $('td:eq(0)', nRow).html(numbering+'.');
            $('td:eq(0), td:eq(1), td:eq(3), td:eq(4), td:eq(6)', nRow).addClass('text-center');
            $('td:eq(2), td:eq(5)', nRow).addClass('text-left');
            
        },
        "responsive":false,
        "scrollX": true,
        "bAutoWidth": true,
        "iDisplayLength": 25,
        "aoColumnDefs": [
            {
                'bSortable': false,
                'aTargets': ['no-sort']
            }
        ],
        "order": [[ 1, 'asc' ]],
        "language": {
            "search": "Pencarian",
            "lengthMenu": "Tampilkan _MENU_ Per Halaman",
            "paginate": {
                "first":      "Awal",
                "last":       "Akhir",
                "next":       "<i class='fa fa-chevron-right'></i>",
                "previous":   "<i class='fa fa-chevron-left'></i>"
            },
            "processing": "<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>",
            "zeroRecords": "Data tidak ditemukan",
            "loadingRecords": "<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>",
            "info": "Menampilkan _START_ - _END_ item dari total _TOTAL_ item",
        },
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": ajax_url,
    });

    var scope_array = ['.filter-metode'];
    $.each(scope_array, function(key, val){
        onchange_filter($(val), scope_array, ajax_url, dataTable);
    });
});
</script>