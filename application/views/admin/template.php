<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $head_title ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo theme_assets('bower_components/bootstrap/dist/css/bootstrap.min.css', 'admin'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo theme_assets('bower_components/font-awesome/css/font-awesome.min.css', 'admin'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo theme_assets('bower_components/Ionicons/css/ionicons.min.css', 'admin'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo theme_assets('dist/css/AdminLTE.min.css', 'admin'); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo theme_assets('dist/css/skins/_all-skins.min.css', 'admin'); ?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo theme_assets('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', 'admin'); ?>">
  
  <link rel="stylesheet" type="text/css" href="<?php echo theme_assets('css/bootstrap-datetimepicker.min.css', 'default'); ?>">

  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo theme_assets('bower_components/bootstrap-daterangepicker/daterangepicker.css', 'admin'); ?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo theme_assets('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css', 'admin'); ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo theme_assets('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css', 'admin'); ?>">
  <!-- jQuery 3 -->
  <script src="<?php echo theme_assets('bower_components/jquery/dist/jquery.min.js', 'admin'); ?>"></script>

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
  .my-swal{
    width:450px !important;
    font-size: 14px !important;
  }
  </style>
</head>
<body class="hold-transition skin-green sidebar-mini fixed sidebar-collapse">
<div class="wrapper">

    <!-- Main Header -->
	<?php $this->load->view("$include_path/header"); ?>
    
    <!-- Left side column. contains the logo and sidebar -->
	<?php $this->load->view("$include_path/sidebar"); ?>

  <!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<?php $this->load->view("$include_path/breadcrumb"); ?>

		<!-- Main content -->
		<section class="content">
			<?php $this->load->view($page); ?>
		</section>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
    
    <!-- Main Footer -->
	<?php $this->load->view("$include_path/footer"); ?>
	<?php $this->load->view("$include_path/modal"); ?>

  
</div>
<!-- ./wrapper -->


<!-- jQuery UI 1.11.4 -->
<script src="<?php echo theme_assets('bower_components/jquery-ui/jquery-ui.min.js', 'admin'); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo theme_assets('bower_components/bootstrap/dist/js/bootstrap.min.js', 'admin'); ?>"></script>
<!-- DataTables -->
<script src="<?php echo theme_assets('bower_components/datatables.net/js/jquery.dataTables.min.js', 'admin'); ?>"></script>
<script src="<?php echo theme_assets('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js', 'admin'); ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo theme_assets('bower_components/moment/min/moment.min.js', 'admin'); ?>"></script>
<script src="<?php echo theme_assets('bower_components/bootstrap-daterangepicker/daterangepicker.js', 'admin'); ?>"></script>
<!-- datepicker -->
<script src="<?php echo theme_assets('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', 'admin'); ?>"></script>

<script src="<?php echo theme_assets('js/bootstrap-datetimepicker.min.js', 'default'); ?>" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo theme_assets('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js', 'admin'); ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo theme_assets('bower_components/jquery-slimscroll/jquery.slimscroll.min.js', 'admin'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo theme_assets('dist/js/adminlte.min.js', 'admin'); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo theme_assets('dist/js/pages/dashboard.js', 'admin'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo theme_assets('dist/js/demo.js', 'admin'); ?>"></script>
<script>
$(document).ready(function(){

  //launch modal 
  $(document).on('click','.modal-view', function(e) {
    e.preventDefault();
    $('#loading').show();
    var modal = $('#main-modal');
      
    if (typeof $(this).attr('modal-size') !== 'undefined') {
      // set modal type (xs, md, lg)
      modal.find('.modal-dialog').attr('class', 'modal-dialog '+$(this).attr('modal-size'));
    } else {
      // set to default (md)
      modal.find('.modal-dialog').attr('class', 'modal-dialog modal-md');
    }
    
    modal.find('.modal-body').load(this.href, function(){
      $('#loading').hide();
      modal.modal('show');
    });
      
    modal.find('.modal-title').html($(this).attr('modal-title'));
  });
  
  //clean modal on close 
  $('#main-modal').on('hidden.bs.modal', function (e){
    $(this).children('div').attr('class','modal-dialog');
    $(this).find('.modal-body').html('');
  });
});

function addPoint(num) {
  var val = num
  return val.toString().replace(/(.)(?=(\d{3})+$)/g,'$1.')
}
</script>
</body>
</html>
