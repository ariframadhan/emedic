<div class="modal fade" id="main-modal" role="dialog" aria-labelledby="modal-box" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<!--Modal header-->
			<div class="modal-header">
				<button data-dismiss="modal" class="close" data-toggle="tooltip" title="close" type="button">
				<span aria-hidden="true"><i class="fa fa-close"></i></span>
				</button>
				<h4 class="modal-title"></h4>
			</div>

			<!--Modal body-->
			<div class="modal-body">
			</div>

		</div>
	</div>
</div>