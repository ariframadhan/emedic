<footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">
		<?php //echo SITE_TITLE ?>
	</div>
	<!-- Default to the left -->
	<strong>&copy; eMedic Indonesia</strong>
</footer>
<script type="text/javascript">
	$(document).ready(function() {
		$.ajaxSetup({
			dataFilter:function(data, type) {
				if (type != 'json') {
					return data;
				}
				var dataObj = $.parseJSON(data);
				if (dataObj.status == 'redirect') {
					window.location.href = dataObj.url;
					return false;
				}else {
					return data;
				}
			}
		});
	});
</script>
