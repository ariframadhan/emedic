<?php
?>
<style>
.smoke {
	background-color: #eee;
}
</style>
<header class="main-header">
	<!-- Logo -->
	<a href="<?php echo base_url('admin'); ?>" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b class="text-white">M</b></span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b class="text-green">e</b>Medic</span>		
	</a>

	<!-- Header Navbar -->
	<nav class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>

		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			
			<ul class="nav navbar-nav">
				<li class="dropdown notifications-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
					<i class="fa fa-bell-o"></i>
					<span class="label label-warning" style="font-size:12px;right:3px;"></span>
				</a>
				<ul class="dropdown-menu">
					<li class="header">You have 0 new notifications</li>
					<!-- inner menu: contains the actual data -->
					
					<li class="footer" style="display:none"><a class="clear-all" href="#">Tandai Semua Terbaca</a></li>
				</ul>
				</li>
				<!-- User Account Menu -->
				<li class="dropdown user">
					<!-- Menu Toggle Button -->
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<!-- hidden-xs hides the username on small devices so only the image appears. -->
						<?php
						// var_dump($logged_in);
							$level = $logged_in['additional_information']['level'];
							$name_id = $logged_in['additional_information']['karyawan']['nm_karyawan'];
							
						?>
						<span class="hidden-xs"><?php echo '<b>'.$name_id.' </b> ('.$level.')'; ?></span>
						<i class="fa fa-caret-down fa-fw"></i>
					</a>
					<ul class="dropdown-menu">
						<li class="footer">
							<a href="<?php echo base_url('admin/user/edit-akun/'.$logged_in['user_id']); ?>" class="modal-view" modal-size="modal-md" modal-title="Pengaturan Akun Saya">Pengaturan Akun</a>
						</li>
						<li class="footer">
							<a href="<?php echo base_url('admin/logout'); ?>">Logout</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
 
</header>
<script>
var base_url = "<?= base_url() ?>";
</script>