<?php 
$level = $logged_in['additional_information']['level'];
// var_dump($logged_in);
?>
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">

      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input id="search-input" type="text" name="q" class="form-control" placeholder="Cari Menu.." autocomplete="off">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
            <a href="<?php echo base_url('admin'); ?>"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a>
        </li>
        <?php if($level == 'ADMIN'): ?>
        <li class="header">DATA SISTEM</li>
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li> -->
        <li>
          <a href="<?php echo base_url('admin/user'); ?>">
            <i class="fa fa-users"></i> <span>Data User</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('admin/ruangan'); ?>">
            <i class="fa fa-hospital-o"></i> <span>Aset Ruangan</span>
          </a>
        </li>
        <?php endif ?>

        <?php if($level == 'ADMIN' || $level == 'OPERATOR-KARYAWAN' || $level == 'OPERATOR-PASIEN'): ?>
          <li class="header">DATA PERSONALIA</li>
          
          <?php if($level == 'ADMIN' || $level == 'OPERATOR-KARYAWAN'): ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-medkit"></i> <span>Data Karyawan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?= base_url('admin/karyawan-medis') ?>"><i class="fa fa-user-md"></i> Karyawan Medis</a></li>
                <li><a href="<?= base_url('admin/karyawan-non-medis') ?>"><i class="fa fa-user-plus"></i> Karyawan Non-Medis</a></li>
              </ul>
            </li>
          <?php endif ?>

          <?php if($level == 'ADMIN' || $level == 'OPERATOR-PASIEN'): ?>
            <li>
              <a href="<?= base_url('admin/pasien') ?>">
                <i class="fa fa-wheelchair"></i> <span>Data Pasien</span>
              </a>
            </li>
          <?php endif ?>

        <?php endif ?>

        <?php if($level == 'ADMIN' || $level == 'OPERATOR-MEDIS'): ?>
          <li class="header">DATA MEDIS</li>
          <li>
            <a href="<?= base_url('admin/perawatan') ?>">
              <i class="fa fa-heartbeat"></i> <span>Perawatan Medis</span>
            </a>
          </li>
          <li>
            <a href="<?= base_url('admin/rawat-inap') ?>">
              <i class="fa fa-hotel"></i> <span>Data Rawat Inap</span>
            </a>
          </li>
        <?php endif ?>

        <?php if($level == 'ADMIN' || $level == 'OPERATOR-KEUANGAN'): ?>
          <li class="header">DATA KEUANGAN</li>
          <li>
            <a href="<?= base_url('admin/keuangan') ?>">
              <i class="fa fa-exchange"></i> <span>Data Transaksi Keuangan</span>
            </a>
          </li>
          <li>
            <a href="<?= base_url('admin/jenis-harga-keuangan') ?>">
              <i class="fa fa-credit-card"></i> <span>Data Jenis dan Harga Keuangan</span>
            </a>
          </li>
        <?php endif ?>

      </ul>
    </section>
    <script>
        $(document).ready(function() {
            var current_url = '<?php echo base_url(uri_string()) ?>';
            // console.log(current_url);
            $('.sidebar-menu li').not('.header').each(function() {
                var attr_url = $(this).children('a').attr('href');
                // console.log(attr_url); 
                if(attr_url == current_url) {
                    $(this).addClass('active');
                    $(this).parents('.treeview').addClass('active');
                // expand treview if menu active
                $(this).parents('ul').css('display', 'block');
            }
        });
        });

        $('#search-input').on('keyup', function () {
            var term = $('#search-input').val().trim();

            if (term.length === 0) {
                $('.sidebar-menu li').each(function () {
                    $(this).show(0);
                    $(this).removeClass('active');
                    if ($(this).data('lte.pushmenu.active')) {
                        $(this).addClass('active');
                    }
                });
                return;
            }

            $('.sidebar-menu li').each(function () {
                if ($(this).text().toLowerCase().indexOf(term.toLowerCase()) === -1) {
                    $(this).hide(0);
                    $(this).removeClass('pushmenu-search-found', false);

                    if ($(this).is('.treeview')) {
                        $(this).removeClass('active');
                    }
                } else {
                    $(this).show(0);
                    $(this).addClass('pushmenu-search-found');

                    if ($(this).is('.treeview')) {
                        $(this).addClass('active');
                    }

                    var parent = $(this).parents('li').first();
                    if (parent.is('.treeview')) {
                        parent.show(0);
                    }
                }

                if ($(this).is('.header')) {
                    $(this).show();
                }
            });

            $('.sidebar-menu li.pushmenu-search-found.treeview').each(function () {
                $(this).find('.pushmenu-search-found').show(0);
            });
        });
    </script>
    <!-- /.sidebar -->
  </aside>
