<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan_harga_model extends MY_Model {

    public $_table      = 'tbl_keuangan_harga';
    public $primary_key = 'id_keuangan_harga';

    protected $return_type = 'array';

    public $belongs_to = array(
        'jenis' => array(
            'model' => 'keuangan_jenis_model',
            'primary_key' => 'id_keuangan_jenis'
        )
    );
    
    public function __construct()
	{
		parent::__construct();
    }

    public function ajax_get_data($where)
    {
        $extractWhere = extract_where_query($where);
        $query = $this->db->query("
            SELECT
                a.*,
                b.nm_jenis
            FROM $this->_table a
            JOIN tbl_keuangan_jenis b ON b.id_keuangan_jenis = a.id_keuangan_jenis
            $extractWhere
        ");

        return $query->result_array();
    }

}
