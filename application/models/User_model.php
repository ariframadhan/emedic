<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model {

    public $_table       = 'tbl_user';
    public $primary_key = 'id_user';

    protected $return_type = 'array';

    public $belongs_to = array(
        'karyawan' => array(
            'model' => 'karyawan_model',
            'primary_key' => 'nik'
        )
    );
    
    public function __construct()
	{
		parent::__construct();
    }
    
    public function ajax_get_data($where)
    {
        $extractWhere = extract_where_query($where);
        $query = $this->db->query("
            SELECT 
                a.*,
                b.nm_karyawan,
                b.email
            FROM $this->_table a
            JOIN tbl_karyawan b ON b.nik = a.nik
            $extractWhere
        ");

        return $query->result_array();
    }

}
