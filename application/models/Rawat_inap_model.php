<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rawat_inap_model extends MY_Model {

    public $_table      = 'tbl_rawat_inap';
    public $primary_key = 'id_rawat_inap';

    protected $return_type = 'array';

    public $belongs_to = array(
        'perawatan' => array(
            'model' => 'perawatan_model',
            'primary_key' => 'id_perawatan'
        ),
        'ruangan' => array(
            'model' => 'ruangan_model',
            'primary_key' => 'id_ruangan'
        )
    );
    
    public function __construct()
	{
		parent::__construct();
    }

    public function ajax_get_data($where)
    {
        $extractWhere = extract_where_query($where);
        $query = $this->db->query("
            SELECT 
                a.*,
                b.id_perawatan,
                c.nm_pasien,
                CONCAT(b.id_perawatan,'|',c.nm_pasien) AS pasien_perawatan,
                d.id_ruangan,
                d.nm_ruangan,
                b.tgl_dirawat
            FROM $this->_table a
            JOIN tbl_perawatan b ON b.id_perawatan = a.id_perawatan
            JOIN tbl_pasien c ON c.id_pasien = b.id_pasien
            LEFT JOIN tbl_ruangan d ON d.id_ruangan = a.id_ruangan
            $extractWhere
        ");

        return $query->result_array();
    }

    public function get_rawat_inap_pending(){
        $query = $this->db->query("
            SELECT * FROM $this->_table WHERE status = 'AKTIF' AND id_ruangan IS NULL
        ");
        $result = $query->result_array();
        return (!empty($result) ? count($result) : 0);
    }

}
