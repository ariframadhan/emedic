<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_model extends MY_Model {

    public $_table      = 'tbl_pasien';
    public $primary_key = 'id_pasien';

    protected $return_type = 'array';

    // public $belongs_to = array(
    //     'bidang' => array(
    //         'model' => 'bidang_kedokteran_model',
    //         'primary_key' => 'id_bidang_kedokteran'
    //     )
    // );
    
    public function __construct()
	{
		parent::__construct();
    }

    public function ajax_get_data($where)
    {
        $extractWhere = extract_where_query($where);
        $query = $this->db->query("
            SELECT 
                a.*
            FROM $this->_table a
            $extractWhere
        ");

        return $query->result_array();
    }

    public function like_query($q) {
        $query = $this->db->select('*')
                ->from($this->_table)
                ->like('nm_pasien',$q,'both')
                ->get();
        return $query->result_array();
	}

}
