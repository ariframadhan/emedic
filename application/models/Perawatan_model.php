<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawatan_model extends MY_Model {

    public $_table      = 'tbl_perawatan';
    public $primary_key = 'id_perawatan';

    protected $return_type = 'array';

    public $belongs_to = array(
        'pasien' => array(
            'model' => 'pasien_model',
            'primary_key' => 'id_pasien'
        ),
        'karyawan' => array(
            'model' => 'karyawan_model',
            'primary_key' => 'id_karyawan'
        )
    );
    
    public function __construct()
	{
		parent::__construct();
    }

    public function ajax_get_data($where)
    {
        $extractWhere = extract_where_query($where);
        $query = $this->db->query("
            SELECT 
                a.*,
                b.nm_pasien,
                c.nm_karyawan
            FROM $this->_table a
            JOIN tbl_pasien b ON b.id_pasien = a.id_pasien
            JOIN tbl_karyawan c ON c.id_karyawan = a.id_karyawan
            $extractWhere
        ");

        return $query->result_array();
    }

}
