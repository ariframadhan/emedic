<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ruangan_model extends MY_Model {

    public $_table      = 'tbl_ruangan';
    public $primary_key = 'id_ruangan';

    protected $return_type = 'array';

    // public $belongs_to = array(
    //     'bidang' => array(
    //         'model' => 'bidang_kedokteran_model',
    //         'primary_key' => 'id_bidang_kedokteran'
    //     )
    // );
    
    public function __construct()
	{
		parent::__construct();
    }

    public function ajax_get_data($where)
    {
        $extractWhere = extract_where_query($where);
        $query = $this->db->query("
            SELECT 
                A.*,
                IF(A.pasien_aktif = A.kapasitas_pasien, 'Full!', IF(A.pasien_aktif > A.kapasitas_pasien, 'Overload!', CONCAT(A.pasien_aktif,' / ',A.kapasitas_pasien))) AS pasien_per_kapasitas 
            FROM (
                SELECT 
                    a.*,
                    (
                        SELECT COUNT(a1.id_rawat_inap)
                        FROM tbl_rawat_inap a1 WHERE a1.id_ruangan = a.id_ruangan AND a1.status = 'AKTIF'
                    ) AS pasien_aktif
                FROM $this->_table a
                $extractWhere
            ) A
        ");

        return $query->result_array();
    }

    public function get_ruangan_tersedia($q){
        $query = $this->db->query("
            SELECT * FROM (
                SELECT 
                    a.*,
                    (
                        SELECT COUNT(a1.id_rawat_inap) FROM
                        tbl_rawat_inap a1
                        WHERE a1.id_ruangan = a.id_ruangan AND a1.status = 'AKTIF'
                    ) AS total_pasien
                FROM $this->_table a) A
            WHERE (A.total_pasien < A.kapasitas_pasien) AND A.nm_ruangan LIKE '%$q%'
        ");
        return $query->result_array();
    }

}
