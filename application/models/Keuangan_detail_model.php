<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan_detail_model extends MY_Model {

    public $_table      = 'tbl_keuangan_detail';
    public $primary_key = 'id_keuangan_detail';

    protected $return_type = 'array';

    public $belongs_to = array(
        'transaksi' => array(
            'model' => 'keuangan_transaksi_model',
            'primary_key' => 'no_transaksi'
        ),
        'jenis' => array(
            'model' => 'keuangan_jenis_model',
            'primary_key' => 'id_keuangan_jenis'
        ),
    );
    
    public function __construct()
	{
		parent::__construct();
    }
    
    public function ajax_get_data($where)
    {
        $extractWhere = extract_where_query($where);
        $query = $this->db->query("
            SELECT 
                a.*,
                b.nm_pasien,
                c.id_user,
                d.nm_karyawan,
                e.total_transaksi,
                f.nm_jenis
            FROM $this->_table a
            JOIN tbl_pasien b ON b.id_pasien = a.id_pasien
            JOIN tbl_user c ON c.id_user = a.id_user
            JOIN tbl_karyawan d ON d.nik = c.nik
            JOIN (
                SELECT 
                    e1.no_transaksi,
                    e1.id_keuangan_jenis,
                    SUM(e1.total) AS total_transaksi
                FROM tbl_keuangan_detail e1
                GROUP BY e1.no_transaksi
            ) e ON e.no_transaksi = a.no_transaksi
            JOIN tbl_keuangan_jenis f ON f.id_keuangan_jenis = e.id_keuangan_jenis
            $extractWhere
        ");

        return $query->result_array();
    }

}
