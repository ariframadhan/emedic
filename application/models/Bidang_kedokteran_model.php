<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidang_kedokteran_model extends MY_Model {

    public $_table      = 'tbl_bidang_kedokteran';
    public $primary_key = 'id_bidang_kedokteran';

    protected $return_type = 'array';

    public $belongs_to = array(
        'cabang' => array(
            'model' => 'cabang_kedokteran_model',
            'primary_key' => 'id_cabang_kedokteran'
        )
    );
    
    public function __construct()
	{
		parent::__construct();
	}

}
