<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends MY_Model {

    public $_table      = 'tbl_karyawan';
    public $primary_key = 'id_karyawan';

    protected $return_type = 'array';

    public $belongs_to = array(
        'bidang' => array(
            'model' => 'bidang_kedokteran_model',
            'primary_key' => 'id_bidang_kedokteran'
        )
    );
    
    public function __construct()
	{
		parent::__construct();
    }
    
    public function ajax_get_data($where)
    {
        $extractWhere = extract_where_query($where);
        $query = $this->db->query("
            SELECT 
                a.*,
                b.nm_bidang_kedokteran,
                c.nm_cabang_kedokteran,
                CONCAT('(',c.nm_cabang_kedokteran,') ',b.nm_bidang_kedokteran) as nm_cabang_bidang_kedokteran
            FROM $this->_table a
            LEFT JOIN tbl_bidang_kedokteran b ON b.id_bidang_kedokteran = a.id_bidang_kedokteran
            LEFT JOIN tbl_cabang_kedokteran c ON c.id_cabang_kedokteran = b.id_cabang_kedokteran
            $extractWhere
        ");

        return $query->result_array();
    }

    public function like_query($q, $jenis_karyawan = null) {
        if(!is_null($jenis_karyawan)){
            $this->db->where('jenis_karyawan', $jenis_karyawan);
        }
        $query = $this->db->select('*')
                ->from($this->_table)
                ->like('nm_karyawan',$q,'both')
                ->get();
        return $query->result_array();
	}

}
