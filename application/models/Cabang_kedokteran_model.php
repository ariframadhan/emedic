<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabang_kedokteran_model extends MY_Model {

    public $_table      = 'tbl_cabang_kedokteran';
    public $primary_key = 'id_cabang_kedokteran';

    protected $return_type = 'array';

    // public $belongs_to = array(
    //     'bidang' => array(
    //         'model' => 'karyawan_model',
    //         'primary_key' => 'nik'
    //     )
    // );
    
    public function __construct()
	{
		parent::__construct();
	}

}
