<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends MY_Controller {

	public function __construct() {
		parent::__construct();

		// set used template path
        $this->_template_path = 'landing';
        
        // Check login session
		if (is_login()) {
            redirect_no_cache('admin');
		}
	}

	//------------------------------------------------------INDEX 140 ---------------------------------//
	public function index()
	{	
		$this->load->view('landing');
	}

}
	