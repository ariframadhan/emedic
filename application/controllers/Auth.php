<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// require_once "PHPMailerAutoload.php";
class Auth extends MY_Controller {

	var $data = array();

	public function __construct()
	{
		parent::__construct();
		if (is_login()) {
			redirect_no_cache(base_url());
		}
		$this->data['page_title'] = SITE_TITLE.' - Login';

		$this->load->library('email');

		$this->load->model('user_model', 'user');
		$this->load->model('karyawan_model', 'karyawan');
		// $this->load->helper('cookie');
	}

	public function index()
	{
		if (!empty($_POST)) {
			// submitted form
			// validate the given data
			$this->load->library('form_validation');
			$this->load->helper('form');

			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');

			$username = $this->input->post('username');

			if (!isset($error)) {
				// verify the given data with
				// the data from database
				$this->load->model('user_model', 'user');
				
				$where['username'] = $this->input->post('username', TRUE);
				$where['password'] = stringEncryption('encrypt',$this->input->post('password', TRUE));
				$user = $this->user->get_by($where);

				//check 
				if ($user == 0) {
					// empty result
					$error = 1;
					$this->data['message'] = create_alert('error', 'Username atau Kata Sandi anda salah');
				}
            }

			// check the result and give a response
			if (!isset($error)) {
		
                $karyawan_data = $this->karyawan->get_by(array('nik'=>$user['nik']));
				$id						= $user['id_user'];
                $data['level']    		= $user['level'];
                $data['karyawan']       = $karyawan_data; 
				
				do_login($id, $username, $data, null);
				// no error
				redirect_no_cache(base_url('admin'));
            }
            
			// $this->data['captcha'] = generate_captcha(4, 'login');
			$this->load->view('login', $this->data);
		} else {
			// show up the form
			$this->load->view('login', $this->data);
		}
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */
