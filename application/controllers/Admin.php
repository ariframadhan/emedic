<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	public function __construct() {
		parent::__construct();

		// set used template path
		$this->_template_path = 'admin';

		// Check login session
		if (!is_login()) {
			if ($this->input->is_ajax_request()) {
				echo json_encode(array('status' => 'redirect', 'url' => base_url('auth')));
			} else {
				redirect_no_cache('auth');
			}
		}

		//load validation config 
		$this->logged_in = $this->session->userdata('logged_in');
		$this->_data['logged_in'] = $this->logged_in;

		$level = $this->logged_in['additional_information']['level'];
		$this->level = $level;

	}

	public function index()
	{	
		$karyawan  = $this->karyawan->get_all();
		$pasien    = $this->pasien->get_all();
		$perawatan = $this->perawatan->get_all();

		$data['total_karyawan']  = !empty($karyawan) ? count($karyawan) : 0;
		$data['total_pasien']    = !empty($pasien) ? count($pasien) : 0;
		$data['total_perawatan'] = !empty($perawatan) ? count($perawatan) : 0;
		
		$this->_load_page('dashboard', 'Dashboard', $data);
	}

	public function logout() 
	{
		do_logout();
		redirect_no_cache('auth');
	}

	public function user($type = null, $param1 = null, $param2 = null)
	{
		$id = $param1;
		switch ($type) {
			case 'delete':
				$this->db->db_debug = false;
				if($this->db->delete('tbl_user', array('id_user'=>$id))){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => 'Anda berhasil hapus data user' 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => 'Anda gagal hapus data user ('.$db_error['message'].')'
					);
					echo json_encode($out);
				}
				break;
			case 'save':
				$data = $this->input->post();

				//processing password encryption
				if($data['password'] != ''){
					$data['password'] = stringEncryption('encrypt', $data['password']);
				}else{
					unset($data['password']);
				}

				// do proccess
				$this->db->db_debug = false;
				$query = $id == null ? $this->db->insert('tbl_user',$data) : $this->db->update('tbl_user', $data, array('id_user'=>$id));
				if($query){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => ($id == null ? 'Anda berhasil input data user baru' : 'Anda berhasil edit data user') 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => $db_error['code'] == '1062' ? 'Data karyawan sudah terdaftar sebagai '.$data['level'] : $db_error['message']
					);
					echo json_encode($out);
				}
				break;
			case 'edit':
			case 'edit-akun':
				$data['data']     = $this->user->with('karyawan')->get_by(array('id_user'=>$id));

				if(empty($data['data'])){
					redirect(404);
				}

				$data['type']     = $type;
				$data['karyawan'] = $this->karyawan->get_all();
				$data['level']    = get_enum_values('tbl_user','level');


				$this->load->view('admin/contents/form-user', $data);
				break;
			case 'add':
				$data['type']     = 'tambah';
				$data['karyawan'] = $this->karyawan->get_all();
				$data['level']    = get_enum_values('tbl_user','level');

				$this->load->view('admin/contents/form-user', $data);
				break;
			case 'get-ajax-data':
				$column      = array('id_user','username','nm_karyawan','email','level','created_at','id_user');
				$where       = array();
			
				// Level
				if($param1 != null && $param1 != 'all'){
					$where['a.level'] = $param1;
				}

				$result      = $this->user->ajax_get_data($where);
				$totalResult = !empty($result) ? count($result) : 0;

				$ouput = array();
				$output['aaData'] = array();
				$output['sEcho'] = intval($_GET['sEcho']);
				$output['iTotalRecords'] = $totalResult;
				$output['iTotalDisplayRecords'] = $totalResult;

				$numbering = $this->db->escape_str($_GET['iDisplayStart']);
				$page = 1;
				foreach ($result as $key => $value)
				{
					$row = array();
					for($i = 0; $i < count($column); $i++)
					{	
						if($i == 0) {
							$row[] = $numbering+$page.'|'.$value[$column[$i]];
						} elseif($i == 4) {
							$lv = $value[$column[$i]];
							$row[] = '<label class="label label-'.($lv == 'ADMIN' ? 'success' : 'primary').'">'.$lv.'</label>';
						} elseif($i == 5) {
							$row[] = generateStringTimestampIndo($value[$column[$i]]);
						} elseif($i == 6) {
							$btn_edit = '<a href="'.base_url('admin/user/edit/'.$value[$column[0]]).'" class="btn btn-default btn-edit modal-view" modal-size="modal-md" modal-title="Edit Data User" data-toggle="tooltip" title="Edit User"><i class="fa fa-edit"></i></a>';

							$btn_delete = '<a class="btn btn-default btn-hapus" data-id-user="'.$value[$column[0]].'" data-toggle="tooltip" title="Hapus User"><i class="fa fa-trash-o"></i></a>';

							$row[] = '<div class="btn-group">'.$btn_edit.$btn_delete.'</div>';
						} else {
							$row[] = $value[$column[$i]];
						}
					}
					$page++;
					$output['aaData'][] = $row;
				}

				echo json_encode($output);
				break;
			case null:
				$data['level_user'] = get_enum_values('tbl_user', 'level');
				$this->_load_page('data-user', 'Data User', $data);
				break;
			
			default:
				redirect(404);
				break;
		}
	}
	
	public function karyawan_non_medis($type = null, $param1 = null, $param2 = null, $param3 = null)
	{		
		$id = $param1;
		switch ($type) {
			case 'delete':
				$this->db->db_debug = false;
				if($this->db->delete('tbl_karyawan', array('id_karyawan'=>$id))){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => 'Anda berhasil hapus data karyawan' 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => 'Anda gagal hapus data karyawan ('.$db_error['message'].')'
					);
					echo json_encode($out);
				}
				break;
			case 'add':
				$data['type']       = 'tambah';
				$data['bidang']     = $this->bidang->with('cabang')->get_all();
				$data['cabang']     = $this->cabang->get_all();
				$data['gender']     = array('Wanita','Pria');
				$data['pendidikan'] = get_enum_values('tbl_karyawan', 'pendidikan');
				$data['status']     = get_enum_values('tbl_karyawan', 'status');

				$this->load->view('admin/contents/form-karyawan', $data);
				break;
			case 'edit':
				$data['data']       = $this->karyawan->get_by(array('id_karyawan'=>$id));

				if(empty($data['data'])){
					redirect(404);
				}

				$data['type']       = 'edit';
				$data['bidang']     = $this->bidang->with('cabang')->get_all();
				$data['cabang']     = $this->cabang->get_all();
				$data['gender']     = array('Wanita','Pria');
				$data['pendidikan'] = get_enum_values('tbl_karyawan', 'pendidikan');
				$data['status']     = get_enum_values('tbl_karyawan', 'status');

				$this->load->view('admin/contents/form-karyawan', $data);
				break;
			case 'save':
				$data = $this->input->post();

				//processing password encryption
				$data['nm_karyawan'] = trim(str_replace('\'','`',strtoupper($data['nm_karyawan'])));
				$data['jenis_karyawan'] = 'NON-MEDIS';
				// do proccess
				$this->db->db_debug = false;
				$query = $id == null ? $this->db->insert('tbl_karyawan',$data) : $this->db->update('tbl_karyawan', $data, array('id_karyawan'=>$id));
				if($query){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => ($id == null ? 'Anda berhasil input data karyawan baru' : 'Anda berhasil edit data karyawan') 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => $db_error['code'] == '1062' ? 'Data karyawan sudah terdaftar sebagai '.$data['level'] : $db_error['message']
					);
					echo json_encode($out);
				}
				break;
			case 'detail':
				$data['data']   = $this->karyawan->with('bidang')->get_by(array('id_karyawan'=>$id));
				$data['data']['cabang'] = array();
				if(!empty($data['data']['bidang'])){
					$data['data']['cabang'] = $this->cabang->get_by(array('id_cabang_kedokteran'=>$data['data']['bidang']['id_cabang_kedokteran']));
				}
				if(empty($data['data'])){
					redirect(404);
				}

				$this->load->view('admin/contents/detail-karyawan',$data);
				break;
			case 'get-ajax-data':
				$column      = array('id_karyawan','nik','nm_karyawan','jenis_kelamin','jabatan','status','tgl_lahir','id_karyawan');
				$where       = array();

				// status
				if($param1 != null && $param1 != 'all'){
					$where['a.status'] = $param1;
				}
				
				// gender
				if($param2 != null && $param2 != 'all'){
					$where['a.jenis_kelamin'] = $param2;
				}
				
				// pendidikan
				if($param3 != null && $param3 != 'all'){
					$where['a.pendidikan'] = $param3;
				}

				$where['a.jenis_karyawan'] = 'NON-MEDIS';
				$result      = $this->karyawan->ajax_get_data($where);
				$totalResult = !empty($result) ? count($result) : 0;

				$ouput = array();
				$output['aaData'] = array();
				$output['sEcho'] = intval($_GET['sEcho']);
				$output['iTotalRecords'] = $totalResult;
				$output['iTotalDisplayRecords'] = $totalResult;

				$numbering = $this->db->escape_str($_GET['iDisplayStart']);
				$page = 1;
				foreach ($result as $key => $value)
				{
					$row = array();
					for($i = 0; $i < count($column); $i++)
					{	
						if($i == 0) {
							$row[] = $numbering+$page.'|'.$value[$column[$i]];
						} else if($i == 1) {
							$row[] = '<a href="'.base_url('admin/karyawan-non-medis/detail/'.$value[$column[0]]).'" class="modal-view" modal-size="modal-md" modal-title="Detail Karyawan" data-toggle="tooltip" title="Detail Karyawan">'.$value[$column[$i]].'</a>';
						}  else if($i == 3){
							$row[] = $value[$column[$i]] ? 'Pria' : 'Wanita';
						} else if($i == 5) {
							$color = array('AKTIF'=>'success','MENGUNDURKAN-DIRI'=>'danger','MANGKIR'=>'danger','DIBERHENTIKAN'=>'danger','MUTASI'=>'warning','PENSIUN'=>'default','MENINGGAL-DUNIA'=>'default','SAKIT-BERKEPANJANGAN'=>'primary');
							$row[] = '<label class="label label-'.$color[$value[$column[$i]]].'">'.$value[$column[$i]].'</label>';
						} else if($i == 6) {
							$row[] = $value[$column[$i]] ? tgl_indo($value[$column[$i]]).' ('.get_umur($value[$column[$i]]).' Tahun)' : '<p class="text-muted">Belum ada data</p>';
						} else if($i == 7) {
							$btn_edit = '<a href="'.base_url('admin/karyawan-non-medis/edit/'.$value[$column[0]]).'" class="btn btn-default btn-edit modal-view" modal-size="modal-lg" modal-title="Edit Data Karyawan" data-toggle="tooltip" title="Edit karyawan"><i class="fa fa-edit"></i></a>';

							$btn_delete = '<a class="btn btn-default btn-hapus" data-id-karyawan="'.$value[$column[0]].'" data-toggle="tooltip" title="Hapus Karyawan"><i class="fa fa-trash-o"></i></a>';

							$row[] = '<div class="btn-group">'.$btn_edit.$btn_delete.'</div>';
						} else {
							$row[] = $value[$column[$i]];
						}
					}
					$page++;
					$output['aaData'][] = $row;
				}

				echo json_encode($output);
				break;
			case null:
				$data['status']     = get_enum_values('tbl_karyawan','status');
				$data['gender']     = array('Wanita','Pria');
				$data['pendidikan'] = get_enum_values('tbl_karyawan','pendidikan');

				$this->_load_page('data-karyawan-non-medis', 'Data Karyawan Non-Medis', $data);
				break;
			
			default:
				redirect(404);
				break;
		}
	}
	
	public function karyawan_medis($type = null, $param1 = null, $param2 = null, $param3 = null)
	{		
		$id = $param1;
		switch ($type) {
			case 'delete':
				$this->db->db_debug = false;
				if($this->db->delete('tbl_karyawan', array('id_karyawan'=>$id))){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => 'Anda berhasil hapus data karyawan' 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => 'Anda gagal hapus data karyawan ('.$db_error['message'].')'
					);
					echo json_encode($out);
				}
				break;
			case 'add':
				$data['type']       = 'tambah';
				$data['bidang']     = $this->bidang->with('cabang')->get_all();
				$data['cabang']     = $this->cabang->get_all();
				$data['gender']     = array('Wanita','Pria');
				$data['pendidikan'] = get_enum_values('tbl_karyawan', 'pendidikan');
				$data['status']     = get_enum_values('tbl_karyawan', 'status');

				$this->load->view('admin/contents/form-karyawan', $data);
				break;
			case 'edit':
				$data['data']       = $this->karyawan->get_by(array('id_karyawan'=>$id));

				if(empty($data['data'])){
					redirect(404);
				}

				$data['type']       = 'edit';
				$data['bidang']     = $this->bidang->with('cabang')->get_all();
				$data['cabang']     = $this->cabang->get_all();
				$data['gender']     = array('Wanita','Pria');
				$data['pendidikan'] = get_enum_values('tbl_karyawan', 'pendidikan');
				$data['status']     = get_enum_values('tbl_karyawan', 'status');

				$this->load->view('admin/contents/form-karyawan', $data);
				break;
			case 'save':
				$data = $this->input->post();

				//processing password encryption
				$data['nm_karyawan'] = trim(str_replace('\'','`',strtoupper($data['nm_karyawan'])));
				$data['jenis_karyawan'] = 'MEDIS';
				// do proccess
				$this->db->db_debug = false;
				$query = $id == null ? $this->db->insert('tbl_karyawan',$data) : $this->db->update('tbl_karyawan', $data, array('id_karyawan'=>$id));
				if($query){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => ($id == null ? 'Anda berhasil input data karyawan baru' : 'Anda berhasil edit data karyawan') 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => $db_error['code'] == '1062' ? 'Data karyawan sudah terdaftar sebagai '.$data['level'] : $db_error['message']
					);
					echo json_encode($out);
				}
				break;
			case 'detail':
				$data['data']   = $this->karyawan->with('bidang')->get_by(array('id_karyawan'=>$id));
				$data['data']['cabang'] = array();
				if(!empty($data['data']['bidang'])){
					$data['data']['cabang'] = $this->cabang->get_by(array('id_cabang_kedokteran'=>$data['data']['bidang']['id_cabang_kedokteran']));
				}
				if(empty($data['data'])){
					redirect(404);
				}

				$this->load->view('admin/contents/detail-karyawan',$data);
				break;
			case 'get-ajax-data':
				$column      = array('id_karyawan','nik','nm_karyawan','jenis_kelamin','nm_cabang_bidang_kedokteran','status','tgl_lahir','id_karyawan');
				$where       = array();

				// status
				if($param1 != null && $param1 != 'all'){
					$where['a.status'] = $param1;
				}
				
				// gender
				if($param2 != null && $param2 != 'all'){
					$where['a.jenis_kelamin'] = $param2;
				}
				
				// pendidikan
				if($param3 != null && $param3 != 'all'){
					$where['a.pendidikan'] = $param3;
				}

				$where['a.jenis_karyawan'] = 'MEDIS';
				$result      = $this->karyawan->ajax_get_data($where);
				$totalResult = !empty($result) ? count($result) : 0;

				$ouput = array();
				$output['aaData'] = array();
				$output['sEcho'] = intval($_GET['sEcho']);
				$output['iTotalRecords'] = $totalResult;
				$output['iTotalDisplayRecords'] = $totalResult;

				$numbering = $this->db->escape_str($_GET['iDisplayStart']);
				$page = 1;
				foreach ($result as $key => $value)
				{
					$row = array();
					for($i = 0; $i < count($column); $i++)
					{	
						if($i == 0) {
							$row[] = $numbering+$page.'|'.$value[$column[$i]];
						} else if($i == 1) {
							$row[] = '<a href="'.base_url('admin/karyawan-medis/detail/'.$value[$column[0]]).'" class="modal-view" modal-size="modal-md" modal-title="Detail Karyawan" data-toggle="tooltip" title="Detail Karyawan">'.$value[$column[$i]].'</a>';
						} else if($i == 3){
							$row[] = $value[$column[$i]] ? 'Pria' : 'Wanita';
						} else if($i == 5) {
							$color = array('AKTIF'=>'success','MENGUNDURKAN-DIRI'=>'danger','MANGKIR'=>'danger','DIBERHENTIKAN'=>'danger','MUTASI'=>'warning','PENSIUN'=>'default','MENINGGAL-DUNIA'=>'default','SAKIT-BERKEPANJANGAN'=>'primary');
							$row[] = '<label class="label label-'.$color[$value[$column[$i]]].'">'.$value[$column[$i]].'</label>';
						} else if($i == 6) {
							$row[] = $value[$column[$i]] ? tgl_indo($value[$column[$i]]).' ('.get_umur($value[$column[$i]]).' Tahun)' : '<p class="text-muted">Belum ada data</p>';
						} else if($i == 7) {
							$btn_edit = '<a href="'.base_url('admin/karyawan-medis/edit/'.$value[$column[0]]).'" class="btn btn-default btn-edit modal-view" modal-size="modal-lg" modal-title="Edit Data Karyawan" data-toggle="tooltip" title="Edit karyawan"><i class="fa fa-edit"></i></a>';

							$btn_delete = '<a class="btn btn-default btn-hapus" data-id-karyawan="'.$value[$column[0]].'" data-toggle="tooltip" title="Hapus Karyawan"><i class="fa fa-trash-o"></i></a>';

							$row[] = '<div class="btn-group">'.$btn_edit.$btn_delete.'</div>';
						} else {
							$row[] = $value[$column[$i]];
						}
					}
					$page++;
					$output['aaData'][] = $row;
				}

				echo json_encode($output);
				break;
			case null:
				$data['status']     = get_enum_values('tbl_karyawan','status');
				$data['gender']     = array('Wanita','Pria');
				$data['pendidikan'] = get_enum_values('tbl_karyawan','pendidikan');

				$this->_load_page('data-karyawan-medis', 'Data Karyawan Medis', $data);
				break;
			
			default:
				redirect(404);
				break;
		}
	}
	
	public function ruangan($type = null, $param1 = null)
	{		
		$id = $param1;
		switch ($type) {
			case 'delete':
				$this->db->db_debug = false;
				if($this->db->delete('tbl_ruangan', array('id_ruangan'=>$id))){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => 'Anda berhasil hapus data ruangan' 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => 'Anda gagal hapus data ruangan ('.$db_error['message'].')'
					);
					echo json_encode($out);
				}
				break;
			case 'add':
				$data['type']       = 'tambah';
	
				$this->load->view('admin/contents/form-ruangan', $data);
				break;
			case 'edit':
				$data['data']       = $this->ruangan->get_by(array('id_ruangan'=>$id));

				if(empty($data['data'])){
					redirect(404);
				}

				$data['type']       = 'edit';
				
				$this->load->view('admin/contents/form-ruangan', $data);
				break;
			case 'save':
				$data = $this->input->post();

				// do proccess
				$this->db->db_debug = false;
				$query = $id == null ? $this->db->insert('tbl_ruangan',$data) : $this->db->update('tbl_ruangan', $data, array('id_ruangan'=>$id));
				if($query){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => ($id == null ? 'Anda berhasil input data ruangan baru' : 'Anda berhasil edit data ruangan') 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => $db_error['code'] == '1062' ? 'Data Ruangan sudah terdaftar sebagai '.$data['level'] : $db_error['message']
					);
					echo json_encode($out);
				}
				break;
			case 'get-ajax-data':
				$column      = array('id_ruangan','nm_ruangan','pasien_per_kapasitas','keterangan','id_ruangan');
				$where       = array();

				$result      = $this->ruangan->ajax_get_data($where);
				$totalResult = !empty($result) ? count($result) : 0;

				$ouput = array();
				$output['aaData'] = array();
				$output['sEcho'] = intval($_GET['sEcho']);
				$output['iTotalRecords'] = $totalResult;
				$output['iTotalDisplayRecords'] = $totalResult;

				$numbering = $this->db->escape_str($_GET['iDisplayStart']);
				$page = 1;
				foreach ($result as $key => $value)
				{
					$row = array();
					for($i = 0; $i < count($column); $i++)
					{	
						if($i == 0) {
							$row[] = $numbering+$page.'|'.$value[$column[$i]];
						} else if($i == 4) {
							$btn_edit = '<a href="'.base_url('admin/ruangan/edit/'.$value[$column[0]]).'" class="btn btn-default btn-edit modal-view" modal-size="modal-md" modal-title="Edit Data Ruangan" data-toggle="tooltip" title="Edit Ruangan"><i class="fa fa-edit"></i></a>';

							$btn_delete = '<a class="btn btn-default btn-hapus" data-id-ruangan="'.$value[$column[0]].'" data-toggle="tooltip" title="Hapus Ruangan"><i class="fa fa-trash-o"></i></a>';

							$row[] = '<div class="btn-group">'.$btn_edit.$btn_delete.'</div>';
						} else {
							$row[] = $value[$column[$i]];
						}
					}
					$page++;
					$output['aaData'][] = $row;
				}

				echo json_encode($output);
				break;
			case null:

				$this->_load_page('data-ruangan', 'Data Aset Ruangan');
				break;
			
			default:
				redirect(404);
				break;
		}
	}
	
	public function pasien($type = null, $param1 = null)
	{		
		$id = $param1;
		switch ($type) {
			case 'delete':
				$this->db->db_debug = false;
				if($this->db->delete('tbl_pasien', array('id_pasien'=>$id))){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => 'Anda berhasil hapus data pasien' 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => 'Anda gagal hapus data pasien ('.$db_error['message'].')'
					);
					echo json_encode($out);
				}
				break;
			case 'add':
				$data['type']     = 'tambah';
				$data['kategori'] = get_enum_values('tbl_pasien','kategori_penyakit'); 
	
				$this->load->view('admin/contents/form-pasien', $data);
				break;
			case 'edit':
				$data['data']       = $this->pasien->get_by(array('id_pasien'=>$id));

				if(empty($data['data'])){
					redirect(404);
				}

				$data['type']     = 'edit';
				$data['kategori'] = get_enum_values('tbl_pasien','kategori_penyakit');
				
				$this->load->view('admin/contents/form-pasien', $data);
				break;
			case 'save':
				$data = $this->input->post();

				// Capitalize name
				$data['nm_pasien'] = strtoupper($data['nm_pasien']);

				// do proccess
				$this->db->db_debug = false;
				$query = $id == null ? $this->db->insert('tbl_pasien',$data) : $this->db->update('tbl_pasien', $data, array('id_pasien'=>$id));
				if($query){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => ($id == null ? 'Anda berhasil input data pasien baru' : 'Anda berhasil edit data pasien') 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => $db_error['code'] == '1062' ? 'Data Pasien sudah terdaftar sebagai '.$data['level'] : $db_error['message']
					);
					echo json_encode($out);
				}
				break;
			case 'get-ajax-data':
				$column      = array('id_pasien','nm_pasien','tgl_lahir','kategori_penyakit','no_telp','alamat','id_pasien');
				$where       = array();

				// kategori
				if($param1 != null && $param1 != 'all'){
					$where['a.kategori_penyakit'] = $param1;
				}

				$result      = $this->pasien->ajax_get_data($where);
				$totalResult = !empty($result) ? count($result) : 0;

				$ouput = array();
				$output['aaData'] = array();
				$output['sEcho'] = intval($_GET['sEcho']);
				$output['iTotalRecords'] = $totalResult;
				$output['iTotalDisplayRecords'] = $totalResult;

				$numbering = $this->db->escape_str($_GET['iDisplayStart']);
				$page = 1;
				foreach ($result as $key => $value)
				{
					$row = array();
					for($i = 0; $i < count($column); $i++)
					{	
						if($i == 0) {
							$row[] = $numbering+$page.'|'.$value[$column[$i]];
						} else if($i == 2) {
							$row[] = $value[$column[$i]] ? tgl_indo($value[$column[$i]]).' ('.get_umur($value[$column[$i]]).' Tahun)' : '<p class="text-muted">Belum ada data</p>';
						} else if($i == 3) {
							$color = array('RINGAN'=>'success','DARURAT'=>'danger','BERAT'=>'warning','TIDAK-DIKETAHUI'=>'default');
							$row[] = '<label class="label label-'.$color[$value[$column[$i]]].'">'.$value[$column[$i]].'</label>';
						} else if($i == 6) {
							$btn_edit = '<a href="'.base_url('admin/pasien/edit/'.$value[$column[0]]).'" class="btn btn-default btn-edit modal-view" modal-size="modal-md" modal-title="Edit Data Pasien" data-toggle="tooltip" title="Edit Pasien"><i class="fa fa-edit"></i></a>';

							$btn_delete = '<a class="btn btn-default btn-hapus" data-id-pasien="'.$value[$column[0]].'" data-toggle="tooltip" title="Hapus Pasien"><i class="fa fa-trash-o"></i></a>';

							$row[] = '<div class="btn-group">'.$btn_edit.$btn_delete.'</div>';
						} else {
							$row[] = $value[$column[$i]];
						}
					}
					$page++;
					$output['aaData'][] = $row;
				}

				echo json_encode($output);
				break;
			case null:
				$data['kategori'] = get_enum_values('tbl_pasien', 'kategori_penyakit');

				$this->_load_page('data-pasien', 'Data Pasien', $data);
				break;
			
			default:
				redirect(404);
				break;
		}
	}
	
	public function perawatan($type = null, $param1 = null, $param2 = null)
	{		
		$id = $param1;
		switch ($type) {
			case 'delete':
				$this->db->db_debug = false;
				if($this->db->delete('tbl_perawatan', array('id_perawatan'=>$id))){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => 'Anda berhasil hapus data perawatan' 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => 'Anda gagal hapus data perawatan ('.$db_error['message'].')'
					);
					echo json_encode($out);
				}
				break;
			case 'add':
				$data['type']     = 'tambah';
				$data['status']   = get_enum_values('tbl_perawatan','status'); 
	
				$this->load->view('admin/contents/form-perawatan', $data);
				break;
			case 'edit':
				$data['data']     = $this->perawatan->with('pasien')->with('karyawan')->get_by(array('id_perawatan'=>$id));

				if(empty($data['data'])){
					redirect(404);
				}

				$data['type']     = 'edit';
				$data['status']   = get_enum_values('tbl_perawatan','status');
				
				$this->load->view('admin/contents/form-perawatan', $data);
				break;
			case 'save':
				$data = $this->input->post();

				// do proccess
				$this->db->db_debug = false;
				$query = $id == null ? $this->db->insert('tbl_perawatan',$data) : $this->db->update('tbl_perawatan', $data, array('id_perawatan'=>$id));
				if($query){
					$out = array(
						'status' => 'success',
						'msg'    => 'Berhasil!',
						'submsg' => ($id == null ? 'Anda berhasil input data perawatan baru' : 'Anda berhasil edit data perawatan') 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => $db_error['code'] == '1062' ? 'Data Perawatan sudah terdaftar sebagai '.$data['level'] : $db_error['message']
					);
					echo json_encode($out);
				}
				break;
			case 'get-pasien':
				// Get data kota then encode to JSON
				$this->_only_ajax_request();
				$keyword = $this->input->get('q', TRUE);
				$result  = $this->pasien->like_query($keyword);
				echo json_encode($result);
				break;
			case 'get-karyawan-medis':
				// Get data kota then encode to JSON
				$this->_only_ajax_request();
				$keyword = $this->input->get('q', TRUE);
				$result  = $this->karyawan->like_query($keyword, 'MEDIS');
				echo json_encode($result);
				break;
			case 'update-status':
				$data = $this->input->post();
				$this->db->db_debug = false;
				if($data['status'] == 'SEMBUH' OR $data['status'] == 'MENINGGAL'){
					$this->db->update('tbl_perawatan', array('tgl_pulang'=>date('Y-m-d h:i:s')), array('id_perawatan'=>$id));
				}else{
					$this->db->update('tbl_perawatan', array('tgl_pulang'=>null), array('id_perawatan'=>$id));
				}
				$query = $this->db->update('tbl_perawatan', $data, array('id_perawatan'=>$id));
				if($query){
					echo json_encode(array('status'=>'success', 'msg'=> 'Berhasil Update Status'));
				}else{
					$error = $this->db->error();
					echo json_encode(array('status'=>'error', 'msg'=> 'Gagal Update Status', 'submsg'=>$error['message']));
				}
				break;
			case 'get-ajax-data':
				$column      = array('id_perawatan','nm_pasien','nm_karyawan','status','tgl_dirawat','tgl_pulang','keterangan','id_perawatan');
				$where       = array();

				// status
				if($param1 != null && $param1 != 'all'){
					$where['a.status'] = $param1;
				}
				
				// karyawan
				if($param2 != null && $param2 != 'all'){
					$where['a.id_karyawan'] = $param2;
				}

				$result      = $this->perawatan->ajax_get_data($where);
				$totalResult = !empty($result) ? count($result) : 0;

				$ouput = array();
				$output['aaData'] = array();
				$output['sEcho'] = intval($_GET['sEcho']);
				$output['iTotalRecords'] = $totalResult;
				$output['iTotalDisplayRecords'] = $totalResult;

				$numbering = $this->db->escape_str($_GET['iDisplayStart']);
				$page = 1;
				foreach ($result as $key => $value)
				{
					$row = array();
					for($i = 0; $i < count($column); $i++)
					{	
						if($i == 0) {
							$row[] = $numbering+$page.'|'.$value[$column[$i]];
						} else if($i == 3) {
							$current = $value[$column[$i]];
							$sts     = get_enum_values('tbl_perawatan', 'status');
							$select  = '<select class="form-control input-sm select-status" data-old-status="'.$current.'" data-id-perawatan="'.$value[$column[0]].'">';
								foreach($sts as $item){
									$select .= '<option value="'.$item.'" '.($item == $current ? 'selected' : '').'>'.$item.'</option>';
								}
							$select  .= '</select>';

							$row[] = $select;

						} else if($i == 4) {
							$timestamps = $value[$column[$i]] ? explode(' ',$value[$column[$i]]) : array();

							$row[] = $timestamps ? tgl_indo($timestamps[0]).' Pukul '.$timestamps[1] : '<p class="text-muted">Belum ada data</p>';
						} else if($i == 5) {
							$timestamps = $value[$column[$i]] ? explode(' ',$value[$column[$i]]) : array();
							
							$row[] = $timestamps ? tgl_indo($timestamps[0]).' Pukul '.$timestamps[1] : '<p class="text-muted">Belum ada data</p>';
						} else if($i == 7) {
							$btn_edit = '<a href="'.base_url('admin/perawatan/edit/'.$value[$column[0]]).'" class="btn btn-default btn-edit modal-view" modal-size="modal-md" modal-title="Edit Data Perawatan" data-toggle="tooltip" title="Edit Perawatan"><i class="fa fa-edit"></i></a>';

							$btn_delete = '<a class="btn btn-default btn-hapus" data-id-perawatan="'.$value[$column[0]].'" data-toggle="tooltip" title="Hapus Perawatan"><i class="fa fa-trash-o"></i></a>';

							$row[] = '<div class="btn-group">'.$btn_edit.$btn_delete.'</div>';
						} else {
							$row[] = $value[$column[$i]];
						}
					}
					$page++;
					$output['aaData'][] = $row;
				}

				echo json_encode($output);
				break;
			case null:
				$data['status']   = get_enum_values('tbl_perawatan', 'status');
				$data['karyawan'] = $this->karyawan->get_many_by(array('jenis_karyawan'=>'MEDIS'));

				$this->_load_page('data-perawatan', 'Data Perawatan Medis', $data);
				break;
			
			default:
				redirect(404);
				break;
		}
	}

	public function rawat_inap($type = null, $param1 = null)
	{		
		$id = $param1;
		switch ($type) {
			case 'tinjau-ruangan':
				if($id == null){
					redirect(404);
				}

				$data['data'] = $this->rawat_inap->with('perawatan')->with('ruangan')->get_by(array('id_rawat_inap'=>$id));
				$data['data']['pasien'] = $this->pasien->get_by(array('id_pasien'=>$data['data']['perawatan']['id_pasien']));

				$this->load->view('admin/contents/form-rawat-inap', $data);
				break;
			case 'get-ruangan-tersedia':
				$this->_only_ajax_request();
				$keyword = $this->input->get('q', TRUE);
				$result  = $this->ruangan->get_ruangan_tersedia($keyword);
				echo json_encode($result);
				break;
			case 'delete':
				$this->db->db_debug = false;
				if($this->db->delete('tbl_rawat_inap', array('id_rawat_inap'=>$id))){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => 'Anda berhasil hapus data rawat inap' 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => 'Anda gagal hapus data rawat inap ('.$db_error['message'].')'
					);
					echo json_encode($out);
				}
				break;

			case 'save':
				$data = $this->input->post();

				if($id == null){
					redirect(404);
				}

				// do proccess
				$this->db->db_debug = false;
				$query = $this->db->update('tbl_rawat_inap', $data, array('id_rawat_inap'=>$id));

				if($query){
					$out = array(
						'status' => 'success',
						'msg'    => 'Berhasil!',
						'submsg' => 'Berhasil Tinjau Ruangan' 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal Tinjau Ruangan!',
						'submsg' => $db_error['message']
					);
					echo json_encode($out);
				}
				break;
			case 'get-ajax-data':
				$column      = array('id_rawat_inap','pasien_perawatan','nm_ruangan', 'tgl_dirawat', 'status', 'id_ruangan');
				$where       = array();

				// status
				if($param1 != null && $param1 != 'all'){
					$where['a.status'] = $param1;
				}

				$result      = $this->rawat_inap->ajax_get_data($where);
				$totalResult = !empty($result) ? count($result) : 0;

				$ouput = array();
				$output['aaData'] = array();
				$output['sEcho'] = intval($_GET['sEcho']);
				$output['iTotalRecords'] = $totalResult;
				$output['iTotalDisplayRecords'] = $totalResult;

				$numbering = $this->db->escape_str($_GET['iDisplayStart']);
				$page = 1;
				foreach ($result as $key => $value)
				{
					$row = array();
					for($i = 0; $i < count($column); $i++)
					{	
						if($i == 0) {
							$row[] = $numbering+$page.'|'.$value[$column[$i]];
						} else if($i == 1) {
							$pasien_perawatan = explode('|', $value[$column[$i]]);
							$row[] = '(Kode : '.$pasien_perawatan[0].') '.$pasien_perawatan[1];
						} else if($i == 2) {
							$row[] = $value[$column[$i]] != null ? $value[$column[$i]] : '<p class="text-danger">Belum Mendapat Ruangan</p>';
						} else if($i == 3){
							$row[] = $value[$column[$i]] != null ? tgl_indo(explode(' ', $value[$column[$i]])[0]).' Pukul '.explode(' ',$value[$column[$i]])[1] : 'Belum Ada Data';
						} else if($i == 4) {
							$row[] = '<label class="label label-'.($value[$column[$i]] == 'AKTIF' ? 'success' : 'primary').'">'.$value[$column[$i]].'</label>'; 
						} else if($i == 5) {
							$id_ruangan    = $value[$column[$i]];
							$id_rawat_inap = $value[$column[0]];

							$btn_tinjau_ruangan = '<a href="'.base_url('admin/rawat-inap/tinjau-ruangan/'.$id_rawat_inap).'" class="btn btn-default btn-tinjau-ruangan modal-view" modal-size="modal-md" modal-title="Tinjau Ruangan Pasien" data-toggle="tooltip" title="Tinjau Ruangan Pasien"><i class="fa fa-hotel"></i></a>';

							$btn_delete = '<a class="btn btn-default btn-hapus" data-id-rawat-inap="'.$id_rawat_inap.'" data-toggle="tooltip" title="Hapus Data Rawat Inap"><i class="fa fa-trash-o"></i></a>';

							$row[] = '<div class="btn-group">'.$btn_tinjau_ruangan.$btn_delete.'</div>';
						} else {
							$row[] = $value[$column[$i]];
						}
					}
					$page++;
					$output['aaData'][] = $row;
				}

				echo json_encode($output);
				break;
			case null:
				$data['status']   = get_enum_values('tbl_rawat_inap', 'status');
				$data['rawat_inap_pending'] = $this->rawat_inap->get_rawat_inap_pending();

				$this->_load_page('data-rawat-inap', 'Data Rawat Inap', $data);
				break;
			
			default:
				redirect(404);
				break;
		}
	}

	public function jenis_harga_keuangan($type = null, $param1 = null)
	{		
		$id = $param1;
		switch ($type) {
			case 'jenis-keuangan':
				$data['data'] = $this->keuangan_jenis->get_all();
				$this->load->view('admin/contents/data-jenis-keuangan', $data);
				break;
			case 'save-jenis':
				$data = $this->input->post();

				// do proccess
				$this->db->db_debug = false;
				$query = $id == null ? $this->db->insert('tbl_keuangan_jenis',$data) : $this->db->update('tbl_keuangan_jenis', $data, array('id_keuangan_jenis'=>$id));
				if($query){
					$out = array(
						'status' => 'success',
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
					);
					echo json_encode($out);
				}
				break;
			case 'delete-jenis':
				if($id == null){
					redirect(404);
				}
				$this->db->db_debug = false;
				if($this->db->delete('tbl_keuangan_jenis', array('id_keuangan_jenis'=>$id))){
					echo json_encode(array('status'=>'success'));
				}else{
					echo json_encode(array('status'=>'error'));
				}
				break;
			case 'delete-harga':
				$this->db->db_debug = false;
				if($this->db->delete('tbl_keuangan_harga', array('id_keuangan_harga'=>$id))){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => 'Anda berhasil hapus data harga keuangan' 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => 'Anda gagal hapus data harga keuangan ('.$db_error['message'].')'
					);
					echo json_encode($out);
				}
				break;
			case 'add-harga':
				$data['type']           = 'tambah';
				$data['jenis_keuangan'] = $this->keuangan_jenis->get_all();
	
				$this->load->view('admin/contents/form-keuangan-harga', $data);
				break;
			case 'edit-harga':
				$data['data']       = $this->keuangan_harga->get_by(array('id_keuangan_harga'=>$id));

				if(empty($data['data'])){
					redirect(404);
				}

				$data['type']           = 'edit';
				$data['jenis_keuangan'] = $this->keuangan_jenis->get_all();
				
				$this->load->view('admin/contents/form-keuangan-harga', $data);
				break;
			case 'save-harga':
				$data = $this->input->post();

				// do proccess
				$this->db->db_debug = false;
				$query = $id == null ? $this->db->insert('tbl_keuangan_harga',$data) : $this->db->update('tbl_keuangan_harga', $data, array('id_keuangan_harga'=>$id));
				if($query){
					$out = array(
						'status' => 'success',
						'msg' => 'Berhasil!',
						'submsg' => ($id == null ? 'Anda berhasil input data harga keuangan baru' : 'Anda berhasil edit data harga keuangan') 
					);
					echo json_encode($out);
				}else{
					$db_error = $this->db->error();
					$out = array(
						'status' => 'error',
						'msg' => 'Gagal!',
						'submsg' => $db_error['message']
					);
					echo json_encode($out);
				}
				break;
			case 'get-ajax-data':
				$column      = array('id_keuangan_harga','nm_jenis','tahun','jumlah','id_keuangan_harga');
				$where       = array();

				$result      = $this->keuangan_harga->ajax_get_data($where);
				$totalResult = !empty($result) ? count($result) : 0;

				$ouput = array();
				$output['aaData'] = array();
				$output['sEcho'] = intval($_GET['sEcho']);
				$output['iTotalRecords'] = $totalResult;
				$output['iTotalDisplayRecords'] = $totalResult;

				$numbering = $this->db->escape_str($_GET['iDisplayStart']);
				$page = 1;
				foreach ($result as $key => $value)
				{
					$row = array();
					for($i = 0; $i < count($column); $i++)
					{	
						if($i == 0) {
							$row[] = $numbering+$page.'|'.$value[$column[$i]];
						} else if($i == 3) {
							$row[] = 'Rp '.str_replace(',','.',number_format($value[$column[$i]]));
						} else if($i == 4) {
							$btn_edit = '<a href="'.base_url('admin/jenis-harga-keuangan/edit-harga/'.$value[$column[0]]).'" class="btn btn-default btn-edit modal-view" modal-size="modal-md" modal-title="Edit Data Harga Keuangan" data-toggle="tooltip" title="Edit Harga Keuangan"><i class="fa fa-edit"></i></a>';

							$btn_delete = '<a class="btn btn-default btn-hapus" data-id-keuangan-harga="'.$value[$column[0]].'" data-toggle="tooltip" title="Hapus Harga Keuangan"><i class="fa fa-trash-o"></i></a>';

							$row[] = '<div class="btn-group">'.$btn_edit.$btn_delete.'</div>';
						} else {
							$row[] = $value[$column[$i]];
						}
					}
					$page++;
					$output['aaData'][] = $row;
				}

				echo json_encode($output);
				break;
			case null:

				$this->_load_page('data-harga-keuangan', 'Data Jenis Harga Keuangan');
				break;
			
			default:
				redirect(404);
				break;
		}
	}

	public function keuangan($type = null, $param1 = null)
	{		
		$id = $param1;
		switch ($type) {
			case 'get-ajax-data':
				$column      = array('no_transaksi','no_transaksi','nm_pasien','tgl_transaksi','metode_pembayaran','total_transaksi','nm_karyawan');
				$where       = array();

				$result      = $this->keuangan_transaksi->ajax_get_data($where);
				$totalResult = !empty($result) ? count($result) : 0;

				$ouput = array();
				$output['aaData'] = array();
				$output['sEcho'] = intval($_GET['sEcho']);
				$output['iTotalRecords'] = $totalResult;
				$output['iTotalDisplayRecords'] = $totalResult;

				$numbering = $this->db->escape_str($_GET['iDisplayStart']);
				$page = 1;
				foreach ($result as $key => $value)
				{
					$row = array();
					for($i = 0; $i < count($column); $i++)
					{	
						if($i == 0) {
							$row[] = $numbering+$page.'|'.$value[$column[$i]];
						} else if($i == 1) {
							$row[] = '<a title="Detail Transaksi" href="'.base_url('admin/keuangan/detail/'.str_replace('/','-',$value[$column[$i]])).'">'.$value[$column[$i]].'</a>';
						} else if($i == 3) {
							$row[] = tgl_indo($value[$column[$i]]);
						} else if($i == 5) {
							$row[] = 'Rp '.str_replace(',','.',number_format($value[$column[$i]]));
						} else {
							$row[] = $value[$column[$i]];
						}
					}
					$page++;
					$output['aaData'][] = $row;
				}

				echo json_encode($output);
				break;
			case null:
				$data['metode_pembayaran'] = get_enum_values('tbl_keuangan_transaksi','metode_pembayaran');
				$data['jenis_keuangan'] = $this->keuangan_harga->with('jenis')->get_many_by(array('tahun'=>date('Y')));

				$this->_load_page('data-keuangan', 'Data Keuangan', $data);
				break;
			
			default:
				redirect(404);
				break;
		}
	}
	
}
	