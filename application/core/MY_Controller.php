<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MY_Controller
 * Base controller class for initialize codeigniter projcet
 * @link  https:/github.com/DykiSA/codeigniter-base-controller
 * @copyright (c) 2016 keraton company <http://keraton.co.id>
 * 
 * created by acing at 01/11/2015 12:01 WIB
 */
class MY_Controller extends CI_Controller {
	/**
	 * Template directory, by default is empty
	 * that mean is located in root of views directory
	 * @var string
	 */
	var $_template_path = '';
	/**
	 * Default data that will sent to view
	 * this used to avoid you to define the data in every function 
	 * @var array
	 */
	var $_data = array();
	/**
	 * Constructor of this class
	 */



	public function __construct()
	{
		parent::__construct();

		// $languages = array('en','id');
		// if(in_array($this->uri->segment(1), $languages)) {
		// 		$this->load->language('front/front_lang', $this->uri->segment(1));
		// }
		// else {
		// 	$segment = $this->uri->segment(1);
		// 	if($segment == null) {
		// 		// redirect to default lang
		// 		redirect(base_url('id'), 'refresh');
		// 	}
		// }

	}
	/**
	 * Load page including the template
	 * @param  string   $page      view file
	 * @param  string   $title     title of page
	 * @param  array    $data      (optional) data will be sent to view
	 * @return view                load 'view' file
	 * the $page is view file that stored under /contents/ directory
	 * so make sure you create the template structure like this
	 * views/
	 *   contents/
	 *   ncludes/
	 *   template.php
	 * you can also create other template under view which structured like this
	 * views/
	 *   template_name/
	 *     contents/
	 *     includes/
	 *     template.php
	 *   other_template_name/
	 *     contents/
	 *     includes/
	 *     template.php
	 * 
	 * if do so, you must spesify the $_template_path in constructor of your controller
	 * NOTE: for more detail see template.php script in /application/views directory
	 */
	public function _load_page($page, $title = '', $data = NULL) {
		$data['page']           = "$this->_template_path/contents/$page";
		$data['include_path']   = "$this->_template_path/includes";
		$data['page_title']     = $title;
		$data['head_title']     = is_null($title) || empty($title) ? constant("SITE_TITLE") :  constant("SITE_TITLE") . " - $title";
		
		// get data from $this->_create_validation_error_result()
		if ($this->load->is_loaded('session')) {
			$data['form_val']   = $this->session->flashdata('form_val');
			$data['form_error'] = $this->session->flashdata('form_error');
		}
		$send_data          = array_merge($this->_data, $data);
		//base_directory return every directory path on view
		return $this->load->view($this->_template_path.'/template', $send_data);
	}
	/**
	 * keep flashdata to next redirect
	 */
	public function _keep_flash_data($key) {
		$value = $this->session->flashdata($key);
		return $this->session->set_flashdata($key, $value);
	}
	/**
	 * generate error message and get value for each field
	 * @param  array $rules  form validation rules
	 * @return array         error which will saved in flashdata 
	 * the function will generate 'form_val' and 'form_error' variable
	 * which will saved in flashdata
	 * the 'form_val' stored the value of each field that was filled
	 * the 'form_error' stored the error message that caused the error
	 * both of their contents has keys that reffered to the name of each fields
	 * by default both of them has sended to view when call '_load_page' function
	 *
	 * NOTE: this feature is used when the form validation is return false
	 */
	public function _create_validation_error_result($rules, $return_type = "flash")
	{
		$this->session->set_flashdata('form_val', $this->input->post());
		$form_error = array();
		foreach ($rules as $config) {
			// get error message every each field 
			$field_error = form_error($config['field']);
			if ($field_error != '') {
				$form_error[$config['field']] = $field_error;
			}
		}
		if($return_type == 'flash') {
			// save error message to flashdata
			$this->session->set_flashdata('form_error', $form_error);
		}
		return $form_error;
	}
	/**
	 * Prevent user to access the function from where this function is called
	 * usually used for deny user to access the method from direct linking
	 * @return error 404 page when is not post request
	 */
	public function _only_post_request()
	{
		if ($this->input->server('REQUEST_METHOD') !== 'POST') {
			$this->_error404();
		}
	}
	/**
	 * Prevent user to access the function from where this function is called
	 * usually used for deny user to access the method from direct linking
	 * @return error 404 page when is not ajax request
	 */
	public function _only_ajax_request()
	{
		if (!$this->input->is_ajax_request()) {
			$this->_error404();
		}
	}
	/**
	 * Prevent anonymous user to access function
	 * @return error 404 page when is anonymous user
	 */
	public function _only_logged_in_user()
	{
		if (!is_login()) {
			$this->_error404();
		}
	}
	/**
	 * delete file under project folder
	 * @param  string $path  path of the file which will be deleted
	 * @return execute file deletion when the given file's path is exists 
	 */
	public function _delete_file($path = NULL)
	{
		if (!is_null($path)) {
			if (file_exists($path)) {
				return unlink(APPPATH.'../'.$path);
			}
		}
		return false;
	}
	/**
	 * Show error 404 not found page
	 * @return view  load error404.php under template folder
	 */
	public function _error404()
	{
		$data['page']          = "$this->_template_path/error404";
		$data['include_path']  = "$this->_template_path/includes";
		$data['page_title']    = 'Error';
		$data['heading']       = 'Error 404 Halaman Tidak Ada';
		$data['head_title']    = constant("SITE_TITLE") . ' - ' . $data['heading'];
		$data['message']       = 'Halaman yang anda minta tidak tersedia';
		$this->output->set_status_header('404');
		$send_data = array_merge($this->_data, $data);

		// echo debug($send_data);
		// echo $this->_template_path;
		echo $this->load->view('error/404', $send_data, TRUE);
		die;
	}

}
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
