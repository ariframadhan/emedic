<?php
/**
 * Global function
 * place for all global function
 */

/*
List Functions :
	- flash_message
	- create_alert
	- do_login
	- is_login
	- do_logout
	- get_logged_in_user
	- is_home
	- create_help_block
	- create_breadcrumb
	- word_limiter
	- redirect_no_cache
	- redirect_cache
	- upload_image
	- parse_time
	- dump
	- debug
	- create_captcha
	- validate_captcha
	- header_no_cache
	- theme_assets
	- dynamic_text_content
*/

$GLOBALS['CI'] =& get_instance();
if (! function_exists('flash_message'))
{
	function flash_message($can_hide = TRUE) {
		global $CI;
		$status = 'success';
		$message = $CI->session->flashdata('success');
		if (empty($message)){
			$message = $CI->session->flashdata('error');
			$status = 'error';
		}

		if (!empty($message)) {
			return create_alert($status, $message, $can_hide);
		}
	}
}

function LoadJpeg($imgname)
{
    /* Attempt to open */
    $im = @imagecreatefromjpeg($imgname);

    /* See if it failed */
    if(!$im)
    {
        /* Create a black image */
        $im  = imagecreatetruecolor(150, 30);
        $bgc = imagecolorallocate($im, 255, 255, 255);
        $tc  = imagecolorallocate($im, 0, 0, 0);

        imagefilledrectangle($im, 0, 0, 150, 30, $bgc);

        /* Output an error message */
        imagestring($im, 1, 5, 5, 'Error loading ' . $imgname, $tc);
    }

    return $im;
}

function LoadPng($imgname)
{
    /* Attempt to open */
    $im = @imagecreatefrompng($imgname);

    /* See if it failed */
    if(!$im)
    {
        /* Create a black image */
        $im  = imagecreatetruecolor(150, 30);
        $bgc = imagecolorallocate($im, 255, 255, 255);
        $tc  = imagecolorallocate($im, 0, 0, 0);

        imagefilledrectangle($im, 0, 0, 150, 30, $bgc);

        /* Output an error message */
        imagestring($im, 1, 5, 5, 'Error loading ' . $imgname, $tc);
    }

    return $im;
}

if (! function_exists('create_alert'))
{
	function create_alert($status, $message, $can_hide = TRUE) {
		global $CI;
		$class = ($status == 'error' ? 'alert-danger' : 'alert-success');
		$title = ($status == 'error' ? 'Error' : 'Success');
		$icon  = ($status == 'error' ? 'fa-ban' : 'fa-check');
		$alert = "<div class=\"alert $class alert-dismissible\">";
		$alert .= $can_hide ? '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' : '';
		$alert .= "<h4><i class=\"icon fa $icon\"></i> $title </h4>$message</div>";
		return $alert;
	}
}
if (! function_exists('do_login'))
{
	function do_login($userid, $username, $additional_information = array(), $type = NULL, $by_email = false)
	{
		global $CI;
		$CI->load->model('user_model', 'user');

		if ($by_email) {
			$username = $CI->user->get_by(array('email'=>$username))['login_user'];
		}

		$arr = array(
			'user_id'                => $userid,
			'user_name'              => $username,
			'time_login'             => date('Y-m-d H:i:s'),
			'additional_information' => $additional_information
		);
		if (!is_null($type)) {
			if ($CI->session->set_userdata('logged_in_'.$type, $arr))
				 return true;
		} else {
			if ($CI->session->set_userdata('logged_in', $arr))
				 return true;
		}
		return false;
	}
}
if (! function_exists('create_breadcrumb'))
{
	function create_breadcrumb() {
		global $CI;
		if(is_home()) {
			return '<li class="active"><i class="fa fa-dashboard"></i> Home</li>';
		}
		else {
			$link = '<li><a href='.base_url('admin').'><i class="fa fa-dashboard"></i> Home</a></li>';
			$url  = $CI->uri->segment_array();
		
			$item = count($url);
			unset($url[0]);
			$num  = 1;
			foreach($url as $segment) {
				if($segment != 'admin') {

					++$num;
					$class = ($num == $item) ? null : 'active';

					if($num == $item) {
						if(strpos($segment, '%20')) {
							$path = str_replace('%20', ' ', $segment);
						}
						else {
							$path = str_replace('-', ' ', $segment);
						}
					}
					else {
						$path = anchor(base_url('admin/'.$segment), str_replace('-', ' ', $segment));
					}

					$link .= '<li class='.$class.'>'.$path.'</li>';
				}
			}
			return $link;
		}
	}
}
 
if (! function_exists('is_login'))
{
	function is_login($type = NULL)
	{
		global $CI;
		$CI->load->library('session');

		if (!is_null($type)) {
			if ($CI->session->userdata('logged_in_'.$type))
				return true;
		} else {
			if ($CI->session->userdata('logged_in'))
				return true;
		}
		return false;
	}
}
if (! function_exists('do_logout'))
{
	function do_logout($type = NULL)
	{
		global $CI;
		if (!is_null($type)) {
			if ($CI->session->unset_userdata('logged_in_'.$type))
				return TRUE;
		} else {
			if ($CI->session->unset_userdata('logged_in'))
				return TRUE;
		}
		return FALSE;
	}
}
if (! function_exists('get_logged_in_user'))
{
	function get_logged_in_user($type = NULL)
	{
		global $CI;
		if (!is_null($type))
			return $CI->session->userdata('logged_in_'.$type);
		else
			return $CI->session->userdata('logged_in');
	}
}
 
if (! function_exists('is_home'))
{
	function is_home() {
		global $CI;
		$segment = $CI->uri->segment(2, '');
		if(empty($segment)) {
			return true;
		}
		return false;
	}
}

if (! function_exists('is_whitelisted'))
{
	function is_whitelisted($nim, $thn_akademik, $kebutuhan) {
		global $CI;
		$CI->load->model('keuangan_whitelist_model', 'whitelist');
		
		if($CI->whitelist->count_by(array('nim'=>$nim,'thn_akademik'=>$thn_akademik,'kebutuhan'=>$kebutuhan))) {
			return true;
		}
		return false;
	}
}

if (! function_exists('ucapan_waktu'))
{
	function ucapan_waktu() {

		date_default_timezone_set("Asia/Jakarta");
		$b = time();
		$hour = date("G",$b);

		if ($hour>=0 && $hour<=11)
		{
			return "Selamat Pagi";
		}
		elseif ($hour >=12 && $hour<=14)
		{
			return "Selamat Siang";
		}
		elseif ($hour >=15 && $hour<=17)
		{
			return "Selamat Sore";
		}
		elseif ($hour >=17 && $hour<=18)
		{
			return "Selamat Petang";
		}
		elseif ($hour >=19 && $hour<=23)
		{
			return "Selamat Malam";
		}else{
			return "Hai";
		}
	}
}

/**
 * Generate Float Random Number
 *
 * @param float $Min Minimal value
 * @param float $Max Maximal value
 * @param int $round The optional number of decimal digits to round to. default 0 means not round
 * @return float Random float value
 */
function float_rand($Min, $Max, $round=0){
	//validate input
	if ($Min > $Max){ 
		$Min = $Max; 
		$Max = $Min; 
	}
    else { 
		$Min = $Min; 
		$Max = $Max; 
	}
	$randomfloat = $Min + mt_rand() / mt_getrandmax() * ($Max - $Min);
	
	if($round > 0){
		$randomfloat = round($randomfloat, $round);
	}

	return $randomfloat;
}

 
if (! function_exists('create_help_block'))
{
	function create_help_block($text)
	{
		$text = str_replace("<p>", "", $text);
		$text = str_replace("</p>", "", $text);
		return '<p class="help-block">'.$text.'</p>';
	}
}

if (! function_exists('redirect_no_cache'))
{
	function redirect_no_cache($url)
	{
		return redirect($url,'refresh',304);
	}
}

if (! function_exists('redirect_cache'))
{
	function redirect_cache($url)
	{
		return redirect($url,'location',302);
	}
}

if (! function_exists('upload_image'))
{
	function upload_file($field_name, $config_name = 'default', $custom_name = false)
	{
		global $CI;

		$CI->load->config('upload');
		$config = $CI->config->item($config_name);
		if($custom_name == false) {
			$config['file_name'] = strtolower($_FILES[$field_name]['name']);
		}
		if (is_array($config) || !empty($config)) {
			$CI->load->library('upload', $config);
			$result = array();
			if($CI->upload->do_upload($field_name))
			{
				$result['result'] = TRUE;
				$result['data']   = $CI->upload->data();
			}
			else
			{
				$result['result'] = FALSE;
				$result['data']   = $CI->upload->display_errors();
			}
		}
		else
		{
			$CI->load->language('upload');
			$result['result'] = FALSE;
			$result['data']   = $CI->lang->line('upload_no_config');	
		}
		return $result;
	}
}


if (! function_exists('debug'))
{
	function debug($data)
	{
		echo '<pre>'.print_r($data, true).'</pre>';
	}
}

if (! function_exists('generate_captcha'))
{
	function generate_captcha($length = 4, $config_name = 'default')
	{
		global $CI;
		$CI->load->helper('captcha');
		$CI->load->helper('string');

		//captcha config
		$prefix = 'captcha_';
		$CI->load->config('captcha');
		$config  = $CI->config->item($prefix.$config_name); 
		
		$captcha = create_captcha($config);
		$data    = array(
			'word'         => $captcha['word'],
			'ip_address'   => $CI->input->ip_address(),
			'captcha_time' => $captcha['time']
		);

	  $CI->session->set_userdata('captcha', $data);
	  return $captcha;
	}
}

if (! function_exists('validate_captcha'))
{
	function validate_captcha($value)
	{
		global $CI;
		$expire       = time() - 360;
		$captcha      = $CI->session->userdata('captcha');
		$word         = $captcha['word'];
		$captcha_time = $captcha['captcha_time'];
		$ip_address   = $captcha['ip_address'];
		$CI->session->unset_userdata('captcha');
		if ($word == $value
			&& $captcha_time > $expire
			&& $ip_address == $CI->input->ip_address()) {
			return true;
		}

		$CI->lang->load('form_validation');
		$CI->form_validation->set_message('validate_captcha', $CI->lang->line('validate_captcha'));
		return false;
	}
}
 
if (! function_exists('header_no_cache'))
{
	function header_no_cache()
	{
		global $CI;
		$CI->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$CI->output->set_header("Pragma: no-cache");
	}
}
 
if (! function_exists('theme_assets'))
{
	function theme_assets($file_path = '', $theme_name = '')
	{
		$asset_folder = 'assets/themes';
		$assets_location = empty($theme_name) ? "$asset_folder/$file_path" : "$asset_folder/$theme_name/$file_path";
		return base_url($assets_location);
	}
}

if (! function_exists('image_assets'))
{
	function image_assets($image_name = '')
	{
		$asset_folder = 'assets/img';
		$assets_location = "$asset_folder/$image_name";
		return base_url($assets_location);
	}
}

/**
 * @using 'OR' you have to specify on the first three character of query  
 * @using '(' or ')' just specify on where's index ex: '(status = 1 OR status = 2)' then you have to write 
 * $where['( a.status ='] = 1;
   $where['OR a.status = )'] = 2;
 * @param  array  $where [description]
 * @return [type]        [description]
 */
function extract_where_query($where = array())
{
	global $CI;
	$extract_where = '';
	if (!empty($where)) {
		foreach ($where as $field => $value) {
			$field = trim($field);
			if ($extract_where == '') {
				$extract_where = 'WHERE ';
			} else {
				if (substr(strtolower($field), 0, 3) == 'or ') {
					$extract_where .= ' OR ';
					$field = str_replace('or ', '', strtolower($field));
				} else {
					$extract_where .= ' AND ';
				}
			}

			if (!preg_match('/(\s|<|>|!|=|is null|is not null)/i', $field)) {
				$operator = ' = ';
			} else {
				$operator = '';
			}
			$pattern = '/(\)$)/';
			if (preg_match($pattern, $field)) {
				$enclosed = ')';
				$field = preg_replace($pattern, '', $field);
			} else {
				$enclosed = '';
			}

			$value = $CI->db->escape_str($value);
			if(is_array($value)) {
				$operator = ') IN ';

				$value_in = '(';
				$total_value = count($value);
				foreach($value as $key => $data) {
					$value_in .= "'".$data."'";
					if($key < $total_value - 1) {
						$value_in .= ',';
					}
				}
				$value_in .= ')';

				$value = $value_in;
				$enclosed = '';
			}
			else {
				$value = "'".$value."'";
			}

			$extract_where .= $field . $operator . $value . $enclosed;
		}
	}
	return $extract_where;
}

/**
 * Crop an image
 * @param  String $src       original file path
 * @param  String $new_image new file path to be placed
 * @param  Integer $x    resize to spesifiec height
 * @param  Integer $y    resize to spesifiec height
 * @param  Integer $width     spesifiec width
 * @param  Integer $height    spesifiec height
 * @return Object            true when success, error message when fail
 */
function crop_image($src, $new_image = null, $x, $y, $width, $height) {
	global $CI;
	$config=array(
		'image_library'		=> 'gd2',
		'source_image'		=> $src,
		'maintain_ratio'	=> false,
		'x_axis'			=> $x,
		'y_axis'			=> $y,
		'quality'			=> '100%',
		'width'				=> $width,
		'height'			=> $height
	);
	if ($new_image != null) {
		$config['new_image'] = $new_image;
	}
	$CI->load->library('image_lib');
	$CI->image_lib->initialize($config);
	if ($CI->image_lib->crop()) {
		return true;
	}else{
		return false;
	};
}

function resize_image($src, $new_image = null, $width, $height) {
	global $CI;
	$config=array(
		'image_library'   => 'gd2',
		'source_image'    => $src,
		'maintain_ration' => true,
		'quality'         => '100%',
		'width'           => $width,
		'height'          => $height,
		'new_image'       => $new_image
	);
	if ($new_image != null) {
		$config['new_image'] = $new_image;
	}
	$CI->load->library('image_lib');
	$CI->image_lib->initialize($config);
	if (!$CI->image_lib->crop()) {
		return false;
	}else{
		return true;
	};
}

function get_enum_values($table, $field) {
	global $CI;
  $type = $CI->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'" )->row( 0 )->Type;
  preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
  $enum = explode("','", $matches[1]);
  return $enum;
}


if (! function_exists("array_key_last")) {
    function array_key_last($array) {
        if (!is_array($array) || empty($array)) {
            return NULL;
        }
       
        return array_keys($array)[count($array)-1];
    }
}


function get_current_semester($ta_berjalan, $tahun_daftar){
	$tahun_berjalan = substr($ta_berjalan, 0, 4);
	$digit_semester = substr($ta_berjalan, -1, 1);
	$selisih_tahun = (int)$tahun_berjalan - (int)$tahun_daftar;
	return (($selisih_tahun * 2) + $digit_semester);
}

function numberToWords($num) {

	$ones = array(
		0 =>"ZERO",
		1 => "ONE",
		2 => "TWO",
		3 => "THREE",
		4 => "FOUR",
		5 => "FIVE",
		6 => "SIX",
		7 => "SEVEN",
		8 => "EIGHT",
		9 => "NINE",
		10 => "TEN",
		11 => "ELEVEN",
		12 => "TWELVE",
		13 => "THIRTEEN",
		14 => "FOURTEEN",
		15 => "FIFTEEN",
		16 => "SIXTEEN",
		17 => "SEVENTEEN",
		18 => "EIGHTEEN",
		19 => "NINETEEN",
		"014" => "FOURTEEN"
	);
	$tens = array( 
		0 => "ZERO",
		1 => "TEN",
		2 => "TWENTY",
		3 => "THIRTY", 
		4 => "FORTY", 
		5 => "FIFTY", 
		6 => "SIXTY", 
		7 => "SEVENTY", 
		8 => "EIGHTY", 
		9 => "NINETY" 
	); 
	$hundreds = array( 
		"HUNDRED", 
		"THOUSAND", 
		"MILLION", 
		"BILLION", 
		"TRILLION", 
		"QUARDRILLION" 
	);
	 /*limit t quadrillion */
	$num = number_format($num,2,".",","); 
	$num_arr = explode(".",$num); 
	$wholenum = $num_arr[0]; 
	$decnum = $num_arr[1]; 
	$whole_arr = array_reverse(explode(",",$wholenum)); 
	krsort($whole_arr,1); 
	$rettxt = ""; 
	foreach($whole_arr as $key => $i){
		
	while(substr($i,0,1)=="0")
			$i=substr($i,1,5);
	if($i < 20){ 
		/* echo "getting:".$i; */
		$rettxt .= $ones[$i]; 
	}elseif($i < 100){ 
		if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
		if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
	}else{ 
		if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
		if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
		if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
	} 
	if($key > 0){ 
	$rettxt .= " ".$hundreds[$key]." "; 
	}
	} 
	if($decnum > 0){
	$rettxt .= " and ";
	if($decnum < 20){
	$rettxt .= $ones[$decnum];
	}elseif($decnum < 100){
	$rettxt .= $tens[substr($decnum,0,1)];
	$rettxt .= " ".$ones[substr($decnum,1,1)];
	}
	}
	return $rettxt;
}


if (! function_exists('is_genap'))
{
	function is_genap($number)
	{
		if($number % 2){
			return false;
		}else{
			return true;
		}
	}
}

function currency($val = '', $currency = '') {
	if($val<0){
		return "($currency".number_format(abs($val), 0, '', '.').")";
	}else{
		return "$currency".number_format($val, 0, '', '.');
	}
}

function delete_file($source) {
	if(is_array($source)) {
		foreach ($source as $path) {
			if(file_exists($path)) {
				unlink($path);
			}
		}
	}
	else {
		if(file_exists($source)) {
			unlink($source);
		}
	}
}

function save_data($data_id = null, $model, $data = array(), $message = array(), $redirect = false){
	global $CI;
	$CI->load->model($model, 'model');
	$CI->db->db_debug = FALSE;

	$data_table 	  = $CI->model->_table;
	$data_primary_key = $CI->model->primary_key;

	if($data_id == null){
		$query = $CI->db->insert($data_table, $data);
	}else{
		$update_key[$data_primary_key] = $data_id;
		$query = $CI->db->update($data_table, $data, $update_key);
	}
	$db_error = $CI->db->error();
	if($query){
		
		$CI->session->set_flashdata('success',$message['success']);
		if(!$redirect){
			echo json_encode(array('status'=>'success','msg'=>$message['success']));
		}else{
			redirect($redirect, 'refresh');
		}

	}else{
		$error_code = $db_error['code'];
		$error_message = $db_error['message'];
		$CI->session->set_flashdata('error', $message['error'].'<br/> <b>Error ['.$error_code.'] : '.$error_message.'</b>');
		
		if(!$redirect){
			echo json_encode(array('status'=>'error','msg'=>$message['error'].'<br/> <b>Error ['.$error_code.'] : '.$error_message.'</b>'));
		}else{
			redirect($redirect, 'refresh');
		}
	}
}

function delete_data($model, $data_id = null, $message = array()){
	global $CI;
	$CI->load->model($model, 'model');
	$CI->db->db_debug = FALSE;

	$data_table 	  = $CI->model->_table;
	$data_primary_key = $CI->model->primary_key;

	$db_error = $CI->db->error();
	$delete_key[$data_primary_key] = $data_id;

	if($CI->db->delete($data_table, $delete_key)){
		
		$CI->session->set_flashdata('success',$message['success']);
		echo json_encode(array('status'=>'success','msg'=>$message['success']));

	}else{
		$error_code = $db_error['code'];
		$error_message = $db_error['message'];
		$CI->session->set_flashdata('error', $message['error'].'<br/> <b>Error ['.$error_code.'] : '.$error_message.'</b>');
		
		echo json_encode(array('status'=>'error','msg'=>$message['error'].'<br/> <b>Error ['.$error_code.'] : '.$error_message.'</b>'));
	}
}

function get_array_duplicate($array) {
	$arr = array_diff_assoc($array, array_unique($array));
	return $arr;
}

function generate_random_string($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function insert_batch_data($model, $data) {
	global $CI;
	$table = $CI->load->model($model, 'model');
	$table = $CI->model->_table;
	$CI->db->insert_batch($table, $data);
	return $CI->db->last_query();
}

function substract_multidimensional_array($source = array(), $filter = array()) {
	$result = array();
	foreach($source as $key => $value) {
		$item = array();
		foreach($filter as $field) {
			$item[$field] = $value[$field];
		}
		$result[] = $item;
	}
	return $result;
}

function compare_multidimensional_array($array1, $array2){
  $difference = array();
  foreach($array1 as $key => $value){
    if(isset($array2[$key])){
      $difference[$key] = abs($array1[$key] - $array2[$key]);
    }else{
      $difference[$key] = $value;
    }
  }
  foreach($array2 as $key => $value){
    if(isset($array1[$key])){
      $difference[$key] = abs($array1[$key] - $array2[$key]);
    }else{
      $difference[$key] = $value;
    }
  }
  return $difference;
}

if (! function_exists('set_session'))
{
	function set_session($session_name, $data)
	{
		global $CI;
		$CI->load->library('session');
		$CI->session->set_userdata($session_name,$data);
		
	}
}

if (! function_exists('get_session'))
{
	function get_session($session_name)
	{
		global $CI;
		$CI->load->library('session');
		return $CI->session->userdata($session_name); 
	}
}
 
if (! function_exists('unformat_currency'))
{
	function unformat_currency($val)
	{
		return (int) filter_var($val, FILTER_SANITIZE_NUMBER_INT);
	}
}


if (!function_exists('fetch_arr')) {
	
	/**
	 * [Get value decrease one level]
	 * @param  [array] $value [description]
	 * @return [array]        [Level Penilaian (bahasa)]
	 */
	function fetch_arr($array) {
		
		if(empty($array)){
			return [];
		} 
		
		$arrayTemp = [];
			foreach ($array as $key => $value) {
				$arrayTemp[] = $value[key($value)];
					# code...
				}

		return $arrayTemp;
	}
}

if (! function_exists('set_notification'))
{
	function set_notification($judul = 'Notifikasi', $message = null, $url = '#', $user_from = null, $user_to = null, $status = 'TERKIRIM')
	{
		global $CI;
		// $CI->load->model('tbl_notifikasi', 'notif');
		$data = array();
		if(is_array($user_to)){
			foreach($user_to as $value){
				$data[] = array(
					'judul' 	=> $judul,
					'message' 	=> $message,
					'url' 		=> $url,
					'user_from' => $user_from,
					'user_to' 	=> $value,
					'status' 	=> $status
				);		
			}
			$CI->db->insert_batch('tbl_notifikasi',$data);
		}else{
			$data = array(
				'judul' 	=> $judul,
				'message' 	=> $message,
				'url' 		=> $url,
				'user_from' => $user_from,
				'user_to' 	=> $user_to,
				'status' 	=> $status
			);
			$CI->db->insert('tbl_notifikasi',$data);
		}
		if ($CI->db->affected_rows() > 0) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
}

if (! function_exists('read_notification'))
{
	function read_notification($id_notifikasi)
	{
		global $CI;
		
		$CI->db->where('id_notifikasi',$id_notifikasi)->update('tbl_notifikasi',array('status'=>'DIBACA'));
		
		if ($CI->db->affected_rows() > 0) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
}

if (! function_exists('clear_notification'))
{
	function clear_notification($id_user)
	{
		global $CI;
		
		$CI->db->where('user_to',$id_user)->where('status','DIBACA')->delete('tbl_notifikasi');
		
		if ($CI->db->affected_rows() > 0) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
}


if (! function_exists('dynamic_content'))
{
	function dynamic_content($data,$model,$id,$additional_data = array())
	{
		global $CI;
		$CI->load->model($model, 'model');
		$primary_key = $CI->model->primary_key;

		$data_model = $CI->model->get_by(array($primary_key => $id));

		$pattern = '[%s]';
			foreach ($data_model as $key => $val) {
					$varMap[sprintf($pattern,$key)] = $val;
			}

			if ($additional_data != '') {
				
				foreach ($additional_data as $keys => $values) {
					$varMap[sprintf($pattern,$keys)] = $values;
				}

			}
		$Content = strtr($data,$varMap);

		return $Content;
	}
}

if (! function_exists('stringEncryption')) 
	{
		
		function stringEncryption($action, $string)
		{
			$output = false;
			
			$encrypt_method = 'AES-256-CBC';                // Default
			$secret_key = '#Yes_w3_Can';               // Change the key!
			$secret_iv = '!IV@_$2';  // Change the init vector!
			
			// hash
			$key = hash('sha256', $secret_key);
			
			// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
			$iv = substr(hash('sha256', $secret_iv), 0, 16);
			
			if( $action == 'encrypt' ) {
					$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
					$output = base64_encode($output);
			}
			else if( $action == 'decrypt' ){
					$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
			}
			
			return $output;
		}
}

function post_to_url($url, $data) {
    $fields = '';
    foreach ($data as $key => $value) {
        $fields .= $key . '=' . $value . '&';
    }
	$fields = rtrim($fields, '&');
	
	// var_dump($url,$data); exit;

    $post = curl_init();

	curl_setopt($post, CURLOPT_URL, $url);
    curl_setopt($post, CURLOPT_POST, count($data));
	curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
	curl_setopt($post, CURLOPT_TIMEOUT, 30);
    curl_setopt($post, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($post);
	$http_code = curl_getinfo($post, CURLINFO_HTTP_CODE);

	if(curl_errno($post)){ //catch if curl error exists and show it
		http_response_code($http_code);
		return 'Curl error[code]: ' . curl_error($post);
	}

    curl_close($post);
    return $result;
}


function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
 
	function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(penyebut($nilai));
		} else {
			$hasil = trim(penyebut($nilai));
		}     		
		return $hasil;
	}

	function tgl_indo($tanggal){
	$tanggal = $tanggal == '0000-00-00' ? '1970-01-01' : $tanggal;
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function hari_indo($indo_tgl){
	$tr   = trim($indo_tgl);
	$indo_tgl    = str_replace(array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'), array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'), $tr);
	return $indo_tgl;
}

function http_translate($code){
	switch ($code) {
		case 100: $text = 'Continue'; break;
		case 101: $text = 'Switching Protocols'; break;
		case 200: $text = 'OK'; break;
		case 201: $text = 'Created'; break;
		case 202: $text = 'Accepted'; break;
		case 203: $text = 'Non-Authoritative Information'; break;
		case 204: $text = 'No Content'; break;
		case 205: $text = 'Reset Content'; break;
		case 206: $text = 'Partial Content'; break;
		case 300: $text = 'Multiple Choices'; break;
		case 301: $text = 'Moved Permanently'; break;
		case 302: $text = 'Moved Temporarily'; break;
		case 303: $text = 'See Other'; break;
		case 304: $text = 'Not Modified'; break;
		case 305: $text = 'Use Proxy'; break;
		case 400: $text = 'Bad Request'; break;
		case 401: $text = 'Unauthorized'; break;
		case 402: $text = 'Payment Required'; break;
		case 403: $text = 'Forbidden'; break;
		case 404: $text = 'Not Found'; break;
		case 405: $text = 'Method Not Allowed'; break;
		case 406: $text = 'Not Acceptable'; break;
		case 407: $text = 'Proxy Authentication Required'; break;
		case 408: $text = 'Request Time-out'; break;
		case 409: $text = 'Conflict'; break;
		case 410: $text = 'Gone'; break;
		case 411: $text = 'Length Required'; break;
		case 412: $text = 'Precondition Failed'; break;
		case 413: $text = 'Request Entity Too Large'; break;
		case 414: $text = 'Request-URI Too Large'; break;
		case 415: $text = 'Unsupported Media Type'; break;
		case 500: $text = 'Internal Server Error'; break;
		case 501: $text = 'Not Implemented'; break;
		case 502: $text = 'Bad Gateway'; break;
		case 503: $text = 'Service Unavailable'; break;
		case 504: $text = 'Gateway Time-out'; break;
		case 505: $text = 'HTTP Version not supported'; break;
		default:
			exit('Unknown http status code "' . htmlentities($code) . '"');
		break;

	}
	return $text;
}

function http_color($code){
	$code = substr($code,0,1);
	switch ($code) {
		case 1:
			return 'text-teal';
			break;
		case 2:
			return 'text-green';
			break;
		case 3:
			return 'text-yellow';
			break;
		case 4:
			return 'text-red';
			break;
		case 5:
			return 'text-muted';
			break;
		
		default:
			return 'text-muted';
			break;
	}
}

// If you wanted to remove the paragraph tags from the HTML
function getFirstParagraph($string){
	$string = substr($string,0, strpos($string, "</p>")+4);
	$string = str_replace("<p>", "", str_replace("<p/>", "", $string));
	return $string;
}

function array_up($arr, $removed_key){
	$new = array();
	foreach($arr as $value) {
		$new[] = $value[$removed_key];
	}
	return $new;
}

function get_multiple_files($input_name){
	$files = array();
	if(!empty($_FILES)){
		foreach($_FILES[$input_name] as $key => $value){
			foreach($value as $key2 => $value2){
				$files['lampiran'.$key2][$key] = $value2;
			}
		}
	}
	return $files;
}

function get_file_extension($file_name) {
	return substr(strrchr($file_name,'.'),1);
}



function is_restricted_file($file_type){
	$arr = array('application/x-python-code', 'text/x-python', 'text/php', 'text/x-php', 'application/php', 'application/x-php', 'application/x-httpd-php', 'application/x-httpd-php-source');
	
	return in_array($file_type, $arr);
}

function get_real_extension($file_type){
	$file_ext = array(
		'application/zip' => 'zip',
		'application/x-rar' => 'rar',
		'application/msword' => 'doc',
		'application/vnd.ms-excel' => 'xls',
		'application/vnd.ms-powerpoint' => 'ppt',
		'text/plain' => 'txt',
		'application/pdf' => 'pdf',
		'audio/mpeg' => 'mp3',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx',
		'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
		'image/png' => 'png',
		'image/jpeg' => 'jpg'
	);

	if(array_key_exists($file_type, $file_ext)){
		return $file_ext[$file_type];
	}else{
		return false;
	}

}

function generate_download_url($model, $path_field, $name_field, $id){
	global $CI;
	$CI->load->model($model, 'model');
	$pk = $CI->model->primary_key;
	$where[$pk] = $id;
	$record = $CI->model->get_by($where);

	return $record[$path_field].$record[$name_field];
}


function sortingHariIndo($a, $b){
	$order = array('SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU', 'MINGGU');
	$key_a = array_search($a['hari'], $order);
    $key_b = array_search($b['hari'], $order);
  
    if($key_a == $key_b){
        return 0;
    }else if($key_a < $key_b){
        return -1; 
    }else{
        return 1;
    }
}

function generateStringTimestampIndo($timestamp){
	$dateTimeData = explode(' ',$timestamp);
	$pukul = 'Pukul '.$dateTimeData[1];

	$hari = hari_indo(date('D', strtotime($dateTimeData[0])));
	$tanggal = tgl_indo($dateTimeData[0]);

	return '<p>'.$hari.', '.$tanggal.', '.$pukul.'</p>';
}

function get_umur($tgl_lahir){
	$tanggal = new DateTime($tgl_lahir);
	$today = new DateTime('today');
	$y = $today->diff($tanggal)->y;

	return $y;
}
























